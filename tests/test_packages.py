import pytest

from deb.debputy.architecture import mock_arch_table
from deb.debputy.buildenv import BuildEnvironment
from deb.debputy.packages import create_binary_package


class MockNamespace:

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)


binary_package_test_data = [
    (
        "arch:any with -i",
        MockNamespace(arch_all=True, arch_any=False, selected_packages=frozenset(), excluded_packages=frozenset()),
        "any",
        "",
        BuildEnvironment(fake_environ={}),
        False
    ),
    (
        "arch:any with -a",
        MockNamespace(arch_all=False, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "any",
        "",
        BuildEnvironment(fake_environ={}),
        True
    ),
    (
        "arch:all with -a",
        MockNamespace(arch_all=False, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "",
        BuildEnvironment(fake_environ={}),
        False
    ),
    (
        "arch:all with no options (or -a + -i)",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "",
        BuildEnvironment(fake_environ={}),
        True
    ),

    (
        "arch:any with -a -Nmscgen",
        MockNamespace(arch_all=False, arch_any=True, selected_packages=frozenset(), excluded_packages={'mscgen'}),
        "any",
        "",
        BuildEnvironment(fake_environ={}),
        False
    ),
    (
        "arch:any with -i -pmscgen",
        MockNamespace(arch_all=True, arch_any=False, selected_packages={'mscgen'}, excluded_packages=frozenset()),
        "any",
        "",
        BuildEnvironment(fake_environ={}),
        True
    ),
    (
        "arch:i386 with -a",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "i386",
        "",
        BuildEnvironment(fake_environ={}),
        False
    ),
    (
        "arch:i386,amd64 with -a",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "i386 amd64",
        "",
        BuildEnvironment(fake_environ={}),
        True
    ),
    (
        "arch:i386 with -pmscgen",
        MockNamespace(arch_all=True, arch_any=False, selected_packages={'mscgen'}, excluded_packages=frozenset()),
        "i386",
        "",
        BuildEnvironment(fake_environ={}),
        False
    ),

    (
        "arch:any with -a, <!nofoo> + foo",
        MockNamespace(arch_all=False, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "any",
        "<!nofoo>",
        BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'foo'}),
        True
    ),
    (
        "arch:any with -a, <!nofoo> + nofoo",
        MockNamespace(arch_all=False, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "<!nofoo>",
        BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'nofoo'}),
        False
    ),
    (
        "arch:any with -a, <...> <!nofoo> + nofoo",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "<bar> <!nofoo>",
        BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'nofoo'}),
        False
    ),
    (
        "arch:any with -a, <...> <... !nofoo> + nofoo",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "<bar> <!nobaz !nofoo>",
        BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'nofoo'}),
        False
    ),
    (
        "arch:any with -a, <...> <... !nofoo> + foo",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "<bar> <!nobaz !nofoo>",
        BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'foo'}),
        True
    ),

    (
        "arch:any with -a, <...> <... foo> + foo",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "<!nobar> <!nobaz !foo>",
        BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'foo'}),
        True
    ),
    (
        "arch:any with -a, <...> <... foo> + nofoo",
        MockNamespace(arch_all=True, arch_any=True, selected_packages=frozenset(), excluded_packages=frozenset()),
        "all",
        "<nobar> <nobaz foo>",
        BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'nofoo'}),
        False
    ),

]


@pytest.mark.parametrize("name,parsed_args,arch_value,build_profile_field,build_env,should_be_acted_on",
                         binary_package_test_data,
                         ids=[t[0] for t in binary_package_test_data]
                         )
def test_binary_package_should_act_on(name, parsed_args, arch_value, build_profile_field, build_env, should_be_acted_on):
    amd64_native_table = mock_arch_table('amd64')
    package_deb822_ctrl = {
        'Package': 'mscgen',
        'Architecture': arch_value,
        'Build-Profiles': build_profile_field,
    }

    binary = create_binary_package(package_deb822_ctrl,
                                   parsed_args.selected_packages,
                                   parsed_args.excluded_packages,
                                   parsed_args.arch_all,
                                   parsed_args.arch_any,
                                   amd64_native_table,
                                   build_env,
                                   1,
                                   )
    assert binary.is_main_package
    assert binary.should_be_acted_on == should_be_acted_on
    if arch_value == "all":
        assert binary.is_arch_all
        assert binary.binary_architecture == "all"
    else:
        assert binary.binary_architecture == amd64_native_table.current_host_arch
        assert not binary.is_arch_all
