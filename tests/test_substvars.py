from deb.debputy.substvar import Substvars


def test_subvars_eq():
    sv1 = Substvars()
    sv2 = Substvars()

    sv1["foo"] = "bar"
    sv1.add_dependency("misc:Depends", "dpkg (>= 1.18.0)")
    sv1.add_dependency("misc:Depends", "debhelper (>= 13)")

    sv2["foo"] = 'bar'
    sv2["misc:Depends"] = "debhelper (>= 13), dpkg (>= 1.18.0)"

    assert sv1 == sv2

    assert sv1["foo"] == "bar"
    assert sv1["misc:Depends"] == "debhelper (>= 13), dpkg (>= 1.18.0)"
    assert sv2["foo"] == "bar"
    assert sv2["misc:Depends"] == "debhelper (>= 13), dpkg (>= 1.18.0)"

    sv2["foo"] = 'baz'
    assert sv1 != sv2

    del sv2["foo"]
    assert sv1 != sv2
