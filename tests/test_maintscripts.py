import textwrap

from deb.debputy.maintscript import MaintscriptSnippet, MaintscriptSnippets


SNIPPET_LOOKUP = {
    'postinst-foo': textwrap.dedent("""\
           if [ -x /usr/bin/foo ]; then
               # do something as a part of postinst
               foo postinst #ARG#
           fi
           """),
    'postrm-foo': textwrap.dedent("""\
       if [ -x /usr/bin/foo ]; then
           # do something as a part of postrm
           foo postrm #ARG#
       fi
       """)
}


def test_maintainer_snippet_inst():
    ms = MaintscriptSnippet()
    assert ms.resolve() == ""
    ms.append_raw("A")
    assert ms.resolve() == "A"
    ms.append_raw("E", snippet_type="service")
    assert ms.resolve() == "AE"
    ms.append_raw("B")
    assert ms.resolve() == "ABE"
    ms.append_raw("F", snippet_type="service")
    ms.append_raw("C")
    ms.append_raw("D")
    assert ms.resolve() == "ABCDEF"


def test_maintainer_snippet_rm():
    ms = MaintscriptSnippet(removal=True)
    assert ms.resolve() == ""
    ms.append_raw("D")
    assert ms.resolve() == "D"
    ms.append_raw("B", snippet_type="service")
    assert ms.resolve() == "BD"
    ms.append_raw("C")
    assert ms.resolve() == "BCD"
    ms.append_raw("A", snippet_type="service")
    assert ms.resolve() == "ABCD"


def test_maintainer_snippets():
    snippets = MaintscriptSnippets(SNIPPET_LOOKUP.__getitem__)
    snippets.add_autoscript("postinst", "postinst-foo", 'debputy/test-suite', variables={'ARG': 'REPLACED'})
    snippets.add_autoscript("postrm", "postrm-foo", 'debputy/test-suite', variables={'ARG': 'REPLACED'})
    postinst = snippets['postinst']
    postrm = snippets['postrm']
    assert snippets['prerm'] == ""
    assert snippets['preinst'] == ""
    assert 'foo postinst REPLACED' in postinst
    assert 'foo postrm REPLACED' in postrm
    assert '# do something as a part of postinst' in postinst
    assert '# do something as a part of postrm' in postrm
    assert '#ARG#' not in postinst
    assert '#ARG#' not in postrm
