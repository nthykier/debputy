import typing

from debian.deb822 import Deb822

from deb.debputy import dhemulation
from deb.debputy.architecture import DpkgArchitectureBuildProcessValuesTable
from deb.debputy.buildenv import BuildEnvironment
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.exceptions import UnknownPackageError, InvalidDebianControlFileError
from deb.debputy.packages import create_binary_package, SourcePackage


def initialize_debputy_from_parsed_args(selected_packages: typing.Set[str],
                                        excluded_packages: typing.Set[str],
                                        select_arch_all: bool,
                                        select_arch_any: bool,
                                        ) -> DebputyInvocationContext:
    """
    Initialize the debputy context from parsed debhelper command line options

    The parameters match the debhelper command line options and the
    documentation here assumes basic understanding of these.

    This function gracefully handles the base case where neither "-p", "-a",
    "-i" are passed means that all packages are selected by default.

    :param selected_packages: The set of packages explicitly requested via "-p" (can be an empty set but not None).
    :param excluded_packages: The set of packages explicitly omitted via "-N" (can be an empty set but not None).
    :param select_arch_all: True if -i was passed. False otherwise.
    :param select_arch_any: True if -a was passed. False otherwise.
    :returns: The context for this invocation
    :raises UnknownPackageError: If any parameter references an unknown
                                 binary package name
    :raises IOError: If there are issues opening or reading debian/control file.
                     Notably, FileNotFoundError if debian/control does not exist.
    :raises InvalidDebianControlFileError: If there issues with the content of
                                           debian/control (e.g. missing fields)

    """
    # If no selection option is set, then all packages are acted on (except the
    # excluded ones)
    if not selected_packages and not select_arch_all and not select_arch_any:
        select_arch_all = True
        select_arch_any = True

    with open("debian/control") as fd:
        dctrl_paragraphs = list(Deb822.iter_paragraphs(fd))

    if len(dctrl_paragraphs) < 2:
        raise InvalidDebianControlFileError("debian/control must contain at least two paragraphs"
                                            " (1 Source + 1-N Package paragraphs)")

    arch_table = DpkgArchitectureBuildProcessValuesTable()
    build_env = BuildEnvironment()
    source_package = SourcePackage(dctrl_paragraphs[0])

    bin_pkgs = [
        create_binary_package(
            p,
            selected_packages,
            excluded_packages,
            select_arch_all,
            select_arch_any,
            arch_table,
            build_env,
            i,
        ) for i, p in enumerate(dctrl_paragraphs[1:], 1)
    ]

    # We do this check late because create_binary_package will handle missing
    # fields (including missing Package fields)
    valid_package_names = {p.package_name for p in bin_pkgs}
    _check_package_sets(selected_packages, valid_package_names, "--package/-p")
    _check_package_sets(excluded_packages, valid_package_names, "--no-package/-N")

    dhemulation.initialize_substvars(bin_pkgs)
    dhemulation.initialize_maintscript_snippets(bin_pkgs)
    dhemulation.initialize_triggers(bin_pkgs)
    build_req = DebputyInvocationContext(build_env, arch_table, source_package, bin_pkgs)
    build_req.ensure_source_date_epoch_is_set()

    return build_req


def _check_package_sets(provided_packages: typing.Set[str], valid_package_names: typing.Set[str], option_name: str):
    if not (provided_packages < valid_package_names):
        non_existing_packages = sorted(provided_packages - valid_package_names)
        invalid_package_list = ", ".join(non_existing_packages)
        msg = f'Invalid package names passed to {option_name}: {invalid_package_list}: ' \
              f'Valid package names are: {", ".join(valid_package_names)}'
        raise UnknownPackageError(msg, option_name, *non_existing_packages)
