import inspect
from typing import Iterable, FrozenSet, Optional

_KNOWN_DEBPUTY_ARGS = {
    'binary_package': None,
    'binary_packages': None,
    'parallel_task_limit': None,
}
_ALL_DECLARED_TASKS = []

SKIP_COMMAND = object()
_REPLACEMENT_TABLE = {
    'dh_testdir': SKIP_COMMAND,
    'dh_testroot': SKIP_COMMAND,
    'create-stamp': SKIP_COMMAND,
}


class DebputyTaskSpec:
    task_name: str
    per_package_task: bool
    replacement_for: Optional[str]
    task_dependencies: FrozenSet[str]
    per_package_dependencies: FrozenSet[str]
    __slots__ = ('task_name', 'per_package_task', 'replacement_for', 'task_implementation', 'task_dependencies',
                 'per_package_dependencies')

    def __init__(self, task_name, per_package_task, dh_tool_name, task_impl, task_dependencies, per_package_dependencies):
        self.task_name = task_name
        self.per_package_task = per_package_task
        self.replacement_for = dh_tool_name
        self.task_implementation = task_impl
        self.task_dependencies = task_dependencies
        self.per_package_dependencies = per_package_dependencies


def debputy_task(task_name: str,
                 implements_external_command: str = None,
                 task_dependencies: Iterable[str] = frozenset(),
                 ):

    def _install_task_handler(func):
        if not inspect.iscoroutinefunction(func):
            raise ValueError("debputy_task must be async defs function")

        spec = DebputyTaskSpec(
            task_name,
            False,
            implements_external_command,
            func,
            frozenset(task_dependencies),
            frozenset(),
        )
        _ALL_DECLARED_TASKS.append(spec)
        if spec.replacement_for is not None:
            _REPLACEMENT_TABLE[spec.replacement_for] = spec

        return func
    return _install_task_handler


def debputy_per_package_task(task_name: str,
                             implements_external_command: str = None,
                             per_package_dependencies: Iterable[str] = frozenset(),
                             task_dependencies: Iterable[str] = frozenset(),
                             ):

    def _install_task_handler(func):
        if not inspect.iscoroutinefunction(func):
            raise ValueError("debputy_per_package_task must be an async def function")

        spec = DebputyTaskSpec(
            task_name,
            True,
            implements_external_command,
            func,
            frozenset(task_dependencies),
            frozenset(per_package_dependencies),
        )
        _ALL_DECLARED_TASKS.append(spec)
        if spec.replacement_for is not None:
            _REPLACEMENT_TABLE[spec.replacement_for] = spec

        return func
    return _install_task_handler


def all_declared_tasks() -> Iterable[DebputyTaskSpec]:
    yield from _ALL_DECLARED_TASKS


def get_replacement_for(tool: str):
    return _REPLACEMENT_TABLE.get(tool)
