import asyncio
import os
import stat
import subprocess

from deb.debputy import cliutil, dhemulation
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.cliutil import CommandFailedError
from deb.debputy.extension import debputy_per_package_task
from deb.debputy.packages import AbstractBinaryPackage


def _clean_path(path):
    if path.startswith(b'\\'):
        path = path[1:].replace(b'\\\\', b'\\')
    return path


async def _to_md5sums(md5sums_stdin_pipe, all_files):
    for filename in all_files:
        wire_format = b'%s\0' % filename.encode('utf-8')
        md5sums_stdin_pipe.write(wire_format)
        await md5sums_stdin_pipe.drain()
    md5sums_stdin_pipe.close()
    await md5sums_stdin_pipe.wait_closed()
    return None


async def _from_md5sums_into_file(md5sums_stdout_pipe, filename):
    with open(filename, 'wb') as fd:
        while 1:
            line = await md5sums_stdout_pipe.readline()
            if line == b'':
                break
            fd.write(_clean_path(line))


def _raise_error(err):
    raise err


def rebased_os_walk(top, *args, **kwargs):
    top = top.rstrip('/')
    prefix_len = len(top) + 1
    for dpath, dirnames, filenames in os.walk(top, *args, **kwargs):
        rebased_dpath = dpath[prefix_len:]
        yield rebased_dpath, dpath, dirnames, filenames


def read_conffiles(binary_package: AbstractBinaryPackage):
    staging_dir = binary_package.staging_dir
    try:
        with open(os.path.join(staging_dir, 'DEBIAN', 'conffiles'), 'rt', encoding="utf-8") as fd:
            for line in fd:
                line = line.strip()
                if line == "":
                    continue
                line = line.lstrip('/')
                yield line
    except FileNotFoundError:
        pass


def all_files_in_staging_dir(staging_dir, exclusions):
    for rebased_dir_path, fs_dir_path, dirnames, filenames in rebased_os_walk(staging_dir,
                                                                              onerror=_raise_error):
        if fs_dir_path == staging_dir:
            # Ignore the DEBIAN directory
            try:
                dirnames.remove('DEBIAN')
            except ValueError:
                pass

        dirnames.sort()
        for filename in sorted(filenames):
            path = os.path.join(fs_dir_path, filename)
            if path not in exclusions and os.path.isfile(path):
                yield os.path.join(rebased_dir_path, filename)


async def generate_md5sums_file(binary_package: AbstractBinaryPackage):
    staging_dir = binary_package.staging_dir
    exclusions = set(read_conffiles(binary_package))
    md5sums_file = os.path.join(staging_dir, 'DEBIAN', 'md5sums')
    os.makedirs(os.path.join(staging_dir, 'DEBIAN'), mode=0o755, exist_ok=True)
    cmd_line = ['xargs', '-r0', 'md5sums']
    cliutil.log_command(*cmd_line, cwd=staging_dir)
    proc = await asyncio.create_subprocess_exec(*cmd_line,
                                                cwd=staging_dir,
                                                stdin=subprocess.PIPE,
                                                stdout=subprocess.PIPE,
                                                )
    awaitables = [
        _to_md5sums(proc.stdin, all_files_in_staging_dir(staging_dir, exclusions)),
        _from_md5sums_into_file(proc.stdout, md5sums_file),
        proc.wait(),
    ]
    await asyncio.gather(*awaitables)
    if proc.returncode != 0:
        raise CommandFailedError(proc.returncode, cmd_line)


async def generate_md5sums_parse_find(binary_package: AbstractBinaryPackage, parallel_task_limit: asyncio.Semaphore):
    staging_dir = binary_package.staging_dir
    skipped_files = set()
    try:
        with open(os.path.join(staging_dir, 'DEBIAN', 'conffiles'), 'r') as fd:
            for line in fd:
                if not line.startswith('/'):
                    continue
                line = line.rstrip().lstrip('/')
                skipped_files.add(line)
    except FileNotFoundError:
        pass
    dhemulation.install_dirs(os.path.join(staging_dir, 'DEBIAN'))
    async with parallel_task_limit:
        shell_cmd = '''xargs -r0 md5sum | perl -pe 'if (s@^\\\\@@) { s/\\\\\\\\/\\\\/g; }' > DEBIAN/md5sums'''
        cliutil.log_shell_command(shell_cmd, cwd=staging_dir)
        md5sums_cmd = await asyncio.create_subprocess_shell(
            shell_cmd,
            cwd=staging_dir,
            stdin=subprocess.PIPE,
            stdout=subprocess.DEVNULL,
            stderr=None,
        )
        strip_prefix = len(staging_dir)
        all_files = []
        for current_dir, dirs, files in os.walk(staging_dir):
            if current_dir == staging_dir:
                try:
                    dirs.remove('DEBIAN')
                except ValueError:
                    pass
            current_dir_stripped = current_dir[strip_prefix:].lstrip('/')
            for filename in files:
                full_path = os.path.join(current_dir, filename)
                st = os.lstat(full_path)
                if not stat.S_ISREG(st.st_mode):
                    continue
                path = os.path.join(current_dir_stripped, filename)
                if path in skipped_files:
                    continue
                all_files.append(path)

        # Bundle up everything and then sort; this matches how dh_md5sums does it
        # and enables us to produce bit-for-bit identical output with debhelper
        # (which in turn simplifies checking if stuff still works).
        #
        # Eventually, it would make sense to sort in the loop above and feed
        # the files to md5sums as they become available (as that is more
        # efficient)
        all_files.sort()
        for path in all_files:
            md5sums_cmd.stdin.write(os.fsencode(path))
            md5sums_cmd.stdin.write(b'\0')
            await md5sums_cmd.stdin.drain()

        md5sums_cmd.stdin.close()
        await md5sums_cmd.stdin.wait_closed()
        await md5sums_cmd.wait()
        if md5sums_cmd.returncode != 0:
            raise OSError("find | md5sums pipeline failed")
        dhemulation.reset_perm(0o644, os.path.join(staging_dir, 'DEBIAN', 'md5sums'))


@debputy_per_package_task(
    'md5sums',
    implements_external_command='dh_md5sums',
    per_package_dependencies=[]
)
async def process_package(binary_package: AbstractBinaryPackage,
                          build_req: DebputyInvocationContext,
                          parallel_task_limit: asyncio.Semaphore
                          ):
    await generate_md5sums_parse_find(binary_package, parallel_task_limit)
    if binary_package.has_dbgsym_binary_package:
        dbgsym_package = binary_package.dbgsym_binary_package
        if os.path.isdir(dbgsym_package.staging_dir):
            await generate_md5sums_parse_find(dbgsym_package, parallel_task_limit)
