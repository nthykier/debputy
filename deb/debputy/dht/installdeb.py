import asyncio
import collections
import os
import re
import subprocess
import textwrap

from deb.debputy import cliutil, dhemulation
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.exceptions import InvalidConfigurationFileInputError, LineReferenceWithPosition
from deb.debputy.extension import debputy_per_package_task
from deb.debputy.internal_globals import STANDARD_WARN_ERROR_LOGGER, STANDARD_OUTPUT_LOGGER
from deb.debputy.maintscript import MaintscriptTemplate
from deb.debputy.packages import AbstractBinaryPackage

MaintscriptValidation = collections.namedtuple('MaintscriptValidation', [
    'min_arg', 'max_arg', 'prior_version_index', 'owning_package_index'
])


DPKG_MAINTSCRIPT_COMMAND_VALIDATION = {
    'rm_conffile': MaintscriptValidation(min_arg=1, max_arg=3, prior_version_index=1, owning_package_index=2),
    'mv_conffile': MaintscriptValidation(min_arg=2, max_arg=3, prior_version_index=2, owning_package_index=3),
    'symlink_to_dir': MaintscriptValidation(min_arg=2, max_arg=4, prior_version_index=2, owning_package_index=3),
    'dir_to_symlink': MaintscriptValidation(min_arg=2, max_arg=4, prior_version_index=2, owning_package_index=3),
}


DEB_PACKAGE_NAME = re.compile('[a-z0-9][-+.a-z0-9]+')
DEB_PACKAGE_VERSION = re.compile(r'''

                 (?: \d+ : )?                # Optional epoch
                 [0-9][0-9A-Za-z.+:~]*       # Upstream version (with no hyphens)
                 (?: - [0-9A-Za-z.+:~]+ )*   # Optional debian revision (+ upstreams versions with hyphens)
''', re.VERBOSE)


def copy_pkgfiles_where_present(binary_package: AbstractBinaryPackage, *pkgfiles, mode=0o644):
    for pkgfilename in pkgfiles:
        pkgfile = dhemulation.package_file(binary_package, pkgfilename)
        if pkgfile:
            target_path = os.path.join(binary_package.staging_dir, 'DEBIAN', pkgfilename)
            dhemulation.install_file(pkgfile, target_path, mode=mode)


async def generate_conffiles(binary_package: AbstractBinaryPackage, parallel_task_limit: asyncio.Semaphore):
    staging_dir = binary_package.staging_dir
    if not os.path.isdir(os.path.join(staging_dir, 'etc')):
        return

    shell_cmd = 'find etc -type f -printf \'/etc/%P\\n\' | LC_ALL=C sort >> DEBIAN/conffiles'

    async with parallel_task_limit:
        cliutil.log_shell_command(shell_cmd, cwd=staging_dir)
        find_cmd = await asyncio.create_subprocess_shell(
            shell_cmd,
            cwd=staging_dir,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=None,
        )
        await find_cmd.wait()
    if find_cmd.returncode != 0:
        STANDARD_WARN_ERROR_LOGGER.error(
            "Command {} failed".format(cliutil.format_command(shell_cmd, cwd=staging_dir))
        )
        raise subprocess.CalledProcessError(
            find_cmd.returncode,
            shell_cmd
        )
    conffiles_path = os.path.join(staging_dir, 'DEBIAN', 'conffiles')

    if os.stat(conffiles_path).st_size == 0:
        # Avoid empty irrelevant empty files
        os.remove(conffiles_path)
    else:
        dhemulation.reset_perm(0o644, conffiles_path)


def handle_install_deb(binary_package: AbstractBinaryPackage, parallel_task_limit: asyncio.Semaphore):
    if binary_package.is_udeb:
        copy_pkgfiles_where_present(binary_package, 'postinst', 'menutest', 'isinstallable', mode=0o755)
        # For udebs, we only install these files and then we are done.
        return

    _install_triggers(binary_package)
    _handle_maintscript_file(binary_package)
    _write_maintscript_files(binary_package)

    return generate_conffiles(binary_package, parallel_task_limit)


def _write_maintscript_files(binary_package: AbstractBinaryPackage):
    snippets = binary_package.maintscript_snippets
    for snippet_name in snippets:
        snippet = snippets[snippet_name]
        provided_script_file = dhemulation.package_file(binary_package, snippet_name)
        if snippet == "" and not provided_script_file:
            continue

        dhemulation.install_dirs(os.path.join(binary_package.staging_dir, 'DEBIAN'))
        target_path = os.path.join(binary_package.staging_dir, 'DEBIAN', snippet_name)
        if provided_script_file:
            with open(provided_script_file) as fd:
                code = fd.read()
            template = MaintscriptTemplate(code)
            payload = template.safe_substitute(PACKAGE=binary_package.package_name, DEBHELPER=snippet)
            STANDARD_OUTPUT_LOGGER.info("   # Creating maintscript %s from %s and generated snippets",
                                        target_path,
                                        provided_script_file
                                        )
        else:
            payload = textwrap.dedent("""\
            #!/bin/sh
            set -e
            """)
            # Do not snippet inside the dedent() as it may contain lines with no whitespace offset, which will break
            # the generated script.
            payload += snippet
            STANDARD_OUTPUT_LOGGER.info("   # Creating maintscript %s solely from generated snippets", target_path)
        with open(target_path, "w") as fd:
            fd.write(payload)
        dhemulation.reset_perm(0o755, target_path)


def _install_triggers(binary_package: AbstractBinaryPackage):
    provided = dhemulation.package_file(binary_package, 'triggers')
    at = binary_package.autotriggers

    if provided is not None and at.empty:
        return
    dhemulation.install_dirs(os.path.join(binary_package.staging_dir, 'DEBIAN'))
    target_path = os.path.join(binary_package.staging_dir, 'DEBIAN', 'triggers')
    write_mode = 'w'
    if provided is not None:
        dhemulation.install_file(provided, target_path)
        write_mode = 'a'
    if not at.empty:
        with open(target_path, write_mode) as fd:
            at.write_triggers(fd)
        if provided is None:
            dhemulation.reset_perm(0o644, target_path)


def _handle_maintscript_file(binary_package: AbstractBinaryPackage):
    maintscript_file = dhemulation.package_file(binary_package, 'maintscript')
    if not maintscript_file:
        return
    snippets = binary_package.maintscript_snippets
    with open(maintscript_file, 'r') as fd:
        for lineno, line in enumerate(fd, start=1):
            stripped_line = line.strip()
            if not stripped_line or stripped_line[0] == '#':
                continue
            cmd_args = _validate_conffile_args(binary_package, line.rstrip(), maintscript_file, lineno)
            cmdline = " ".join(cmd_args)
            STANDARD_OUTPUT_LOGGER.info("   # Injecting script snippet for %s (%s, from: %s:%d)",
                                        binary_package.package_name,
                                        cmd_args[0],
                                        maintscript_file,
                                        lineno,
                                        )
            for script in ('postinst', 'preinst', 'prerm', 'postrm'):
                snippets.add_autoscript(script,
                                        'maintscript-helper',
                                        'debuty-installdeb',
                                        variables={'PARAMS': cmdline}
                                        )


def _validate_conffile_args(binary_package: AbstractBinaryPackage, line_content: str, filename: str, lineno: int):
    command_with_args = line_content.split()
    command, *args = command_with_args
    if '--' in args:
        m = re.search(r'--(?:\s|$)', line_content)
        assert m
        loc_ref = LineReferenceWithPosition.line_ref_from_re_match(filename,
                                                                   lineno,
                                                                   line_content,
                                                                   m,
                                                                   )
        raise InvalidConfigurationFileInputError(
            f'The maintscripts file for {binary_package.package_name} includes a "--" for the {command} command,'
            ' but it must not',
            loc_ref)

    try:
        validation = DPKG_MAINTSCRIPT_COMMAND_VALIDATION[command]
    except KeyError:
        validation = None

    if validation and validation.min_arg is not None and len(args) < validation.min_arg:
        loc_ref = LineReferenceWithPosition.loc_ref_entire_line(filename, lineno, line_content)
        raise InvalidConfigurationFileInputError(
            f'The "{command}" command at least {validation.min_arg} parameters but only given {len(args)}',
            loc_ref
        )
    if validation and validation.max_arg is not None and len(args) > validation.max_arg:
        re_str = re.escape(command) + r'(?:\s+\S+){' + str(validation.max_arg) + r'}\s+(\S.*)$'
        m = re.search(re_str, line_content)
        loc_ref = LineReferenceWithPosition.line_ref_from_re_match(filename,
                                                                   lineno,
                                                                   line_content,
                                                                   m,
                                                                   )
        raise InvalidConfigurationFileInputError(
            f'The "{command}" command at most {validation.max_arg} parameters but was given {len(args)}',
            loc_ref
        )

    if validation and validation.prior_version_index is not None and validation.prior_version_index < len(args) \
            and not DEB_PACKAGE_VERSION.match(args[validation.prior_version_index]):
        loc_ref = LineReferenceWithPosition.loc_ref_from_word_no(filename,
                                                                 lineno,
                                                                 line_content,
                                                                 # +2 (one for command and one for index vs. pos)
                                                                 validation.prior_version_index + 2
                                                                 )

        raise InvalidConfigurationFileInputError(
            f'Argument {validation.prior_version_index} for "{command}" command must be a valid version number',
            loc_ref
        )

    if validation and validation.owning_package_index is not None and validation.owning_package_index < len(args) \
            and not DEB_PACKAGE_NAME.match(args[validation.owning_package_index]):
        loc_ref = LineReferenceWithPosition.loc_ref_from_word_no(filename,
                                                                 lineno,
                                                                 line_content,
                                                                 # +2 (one for command and one for index vs. pos)
                                                                 validation.owning_package_index + 2
                                                                 )
        raise InvalidConfigurationFileInputError(
            f'Argument {validation.prior_version_index} for "{command}" command must be a valid package name',
            loc_ref
        )
    # FIXME: Add missing validation from dh_installdeb
    return command_with_args


@debputy_per_package_task(
    'installdeb',
    implements_external_command='dh_installdeb',
    per_package_dependencies=[]
)
async def process_package(binary_package: AbstractBinaryPackage,
                          build_req: DebputyInvocationContext,
                          parallel_task_limit: asyncio.Semaphore
                          ):
    await handle_install_deb(binary_package, parallel_task_limit)
    if binary_package.has_dbgsym_binary_package:
        dbgsym_package = binary_package.dbgsym_binary_package
        if os.path.isdir(dbgsym_package.staging_dir):
            await handle_install_deb(dbgsym_package, parallel_task_limit)
