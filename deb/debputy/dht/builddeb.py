import asyncio
import os
import re

from deb.debputy import cliutil
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.extension import debputy_per_package_task
from deb.debputy.internal_globals import DBGSYM_PACKAGE_TYPE, DEFAULT_PACKAGE_TYPE, UDEB_PACKAGE_TYPE, \
    STANDARD_OUTPUT_LOGGER
from deb.debputy.packages import AbstractBinaryPackage


def rename_ext_sub_generator(old_extension_re, new_extension):
    def _rename_sub(name):
        return old_extension_re.sub(new_extension, name)
    return _rename_sub


DEFAULT_EXTENSION_RE = re.compile(r'[.]{}$'.format(re.escape(DEFAULT_PACKAGE_TYPE)))
RENAME_UDEB_SUB = rename_ext_sub_generator(DEFAULT_EXTENSION_RE, UDEB_PACKAGE_TYPE)
RENAME_DBGSYM_SUB = None
if DBGSYM_PACKAGE_TYPE != DEFAULT_PACKAGE_TYPE:
    RENAME_DBGSYM_SUB = rename_ext_sub_generator(DEFAULT_EXTENSION_RE, DBGSYM_PACKAGE_TYPE)


async def build_deb(destdir, partial_dpkg_cmd, parallel_task_limit: asyncio.Semaphore, *, rename_sub=None, tmp_assembly_dir=None):
    if rename_sub is None:
        # Simple case
        await cliutil.run_cmd(*partial_dpkg_cmd, destdir, parallel_task_limit=parallel_task_limit)
        return

    if tmp_assembly_dir is None:
        raise ValueError("tmp_assembly_dir must not be None when rename_sub is given")

    # Rename case
    await cliutil.run_cmd(*partial_dpkg_cmd, tmp_assembly_dir, parallel_task_limit=parallel_task_limit)
    generated_files = os.listdir(tmp_assembly_dir)
    if len(generated_files) != 1:
        cmd_line = cliutil.format_command(partial_dpkg_cmd, tmp_assembly_dir)
        assert generated_files, "%s did not produce any deb but was successful!?" % cmd_line
        assert not generated_files, "%s produced multiple output files: %s" % (cmd_line, str(generated_files))

    produced_file = generated_files[0]
    desired_filename = rename_sub(produced_file)
    assert desired_filename, "Renaming sub did not return a new (better) name!?"
    if desired_filename != produced_file:
        STANDARD_OUTPUT_LOGGER.info("\tRenaming %s to %s", produced_file, desired_filename)
    orig_path = os.path.join(tmp_assembly_dir, produced_file)
    desired_path = os.path.join(destdir, desired_filename)
    os.rename(orig_path, desired_path)


def _dpkg_cmd(staging_dir, *, build_type_default_options=None):
    dpkg_cmd_line = ['dpkg-deb']
    if build_type_default_options:
        dpkg_cmd_line.extend(build_type_default_options)
    # dpkg_cmd_line.extend(self.default_dpkg_options)
    # dpkg_cmd_line.extend(self.requested_dpkg_options)
    dpkg_cmd_line.append('--root-owner-group')
    dpkg_cmd_line.append('--build')
    dpkg_cmd_line.append(staging_dir)
    return dpkg_cmd_line


@debputy_per_package_task(
    'builddeb',
    implements_external_command='dh_builddeb',
    per_package_dependencies=['gencontrol', 'md5sums', 'installdeb', 'shlibdeps']
)
async def process_package(binary_package: AbstractBinaryPackage, build_req: DebputyInvocationContext, parallel_task_limit: asyncio.Semaphore, destdir=".."):
    if binary_package.is_udeb:
        scratch_space_udeb = "debian/.debhelper/scratch-space/build-%s-%s" % (
            UDEB_PACKAGE_TYPE, binary_package.package_name)
        await build_deb(destdir,
                        _dpkg_cmd(binary_package.staging_dir,
                                  build_type_default_options=['-z6', '-Zxz', '-Sextreme']),
                        parallel_task_limit,
                        rename_sub=RENAME_DBGSYM_SUB,
                        tmp_assembly_dir=scratch_space_udeb)
    else:
        await build_deb(destdir, _dpkg_cmd(binary_package.staging_dir), parallel_task_limit)
        if not binary_package.has_dbgsym_binary_package:
            return
        dbgsym_staging_dir = binary_package.dbgsym_binary_package.staging_dir
        if os.path.exists(os.path.join(dbgsym_staging_dir, 'DEBIAN', 'control')):
            scratch_space_dbgsym = "debian/.debhelper/scratch-space/build-%s-%s" % (
                DBGSYM_PACKAGE_TYPE, binary_package.package_name)
            await build_deb(destdir,
                            _dpkg_cmd(dbgsym_staging_dir),
                            parallel_task_limit,
                            rename_sub=RENAME_DBGSYM_SUB,
                            tmp_assembly_dir=scratch_space_dbgsym)
