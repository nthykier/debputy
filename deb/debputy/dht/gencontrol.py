import asyncio
import os

from deb.debputy import dhemulation, cliutil
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.extension import debputy_per_package_task
from deb.debputy.internal_globals import DBGSYM_PACKAGE_TYPE, DEFAULT_PACKAGE_TYPE
from deb.debputy.packages import AbstractBinaryPackage, DbgsymBinaryPackage, BinaryPackage

DBGSYM_CLEARED_FIELDS = [
    'Pre-Depends', 'Recommends', 'Suggests', 'Enhances', 'Provides', 'Essential', 'Conflicts', 'Homepage',
    'Important', 'Built-Using'
]
DBGSYM_STATIC_FIELDS = {
    'Priority': 'optional',
    'Auto-Built-Package': 'debug-symbols',
    'Package': '{dbgsym_package.package_name}',
    'Depends': '{parent_package.package_name} (= ${{binary:Version}})',
    'Description': 'debug symbols for {parent_package.package_name}',
    'Section': '{dbgsym_package.archive_section}',
}
STD_SUBSTVARS = ['misc:Depends', 'misc:Pre-Depends']


def ensure_substvars_present(binary_package: AbstractBinaryPackage, substvars_file):
    substvars = binary_package.substvars
    for substvar_name in STD_SUBSTVARS:
        if substvar_name not in substvars:
            substvars[substvar_name] = ''
    substvars.write_substvars(substvars_file)


def parse_dbgsym_build_id_file(dbgsym_package: DbgsymBinaryPackage):
    path = os.path.join(dbgsym_package.dbgsym_info_dir, 'dbgsym-build-ids')
    try:
        with open(path, 'r', encoding="utf-8") as fd:
            seen = set()
            for dbg_id in sorted(fd.read().split()):
                if dbg_id not in seen:
                    seen.add(dbg_id)
                    yield dbg_id
    except FileNotFoundError:
        pass


def parse_dbgsym_migration_file(dbgsym_package: DbgsymBinaryPackage):
    path = os.path.join(dbgsym_package.dbgsym_info_dir, 'dbgsym-migration')
    try:
        with open(path, 'r', encoding='utf-8') as fd:
            return fd.read().strip()
    except FileNotFoundError:
        pass
    return None


async def invoke_dpkg_gencontrol(binary_package: AbstractBinaryPackage, dpkg_gentrol_cmd,
                                 parallel_task_limit: asyncio.Semaphore):
    staging_dir = binary_package.staging_dir
    os.makedirs(os.path.join(staging_dir, 'DEBIAN'), mode=0o755, exist_ok=True)
    await cliutil.run_cmd(*dpkg_gentrol_cmd, parallel_task_limit=parallel_task_limit)


def _all_dbgsym_fields(binary_package: DbgsymBinaryPackage):
    yield from ("-U%s" % field for field in DBGSYM_CLEARED_FIELDS)
    for field, value in DBGSYM_STATIC_FIELDS.items():
        if '{' in value:
            value = value.format(dbgsym_package=binary_package,
                                 parent_package=binary_package.parent_package)
        yield "-D%s=%s" % (field, value)


def _dpkg_cmd(binary_package: AbstractBinaryPackage,
              package_name_in_dctrl,
              substvars_file,
              include_build_id_in_control_file=False
              ):
    dpkg_cmd_line = ['dpkg-gencontrol',
                     "-p%s" % package_name_in_dctrl,
                     "-l%s" % dhemulation.package_file(binary_package,
                                                       'changelog',
                                                       mandatory=True,
                                                       always_fallback_to_packageless_variant=True,
                                                       ),
                     "-T%s" % substvars_file,
                     "-P%s" % binary_package.staging_dir,
                     ]

    clear_ma_value = False

    if isinstance(binary_package, DbgsymBinaryPackage):
        dpkg_cmd_line.extend(_all_dbgsym_fields(binary_package))
        if DBGSYM_PACKAGE_TYPE != DEFAULT_PACKAGE_TYPE:
            dpkg_cmd_line.append('-DPackage-Type=%s' % DBGSYM_PACKAGE_TYPE)
        migration = parse_dbgsym_migration_file(binary_package)
        if migration:
            dpkg_cmd_line.append('-DReplaces=%s' % migration)
            dpkg_cmd_line.append('-DBreaks=%s' % migration)
        else:
            dpkg_cmd_line.append('-UReplaces')
            dpkg_cmd_line.append('-UBreaks')
        # For dbgsym, only "M-A: same" is kept as none of the other values make sense.
        clear_ma_value = binary_package.package_fields.get('multi-arch', 'unset') != 'same'
        # We always include build-id in the control file for dbgsym packages
        include_build_id_in_control_file = True
    elif binary_package.package_fields.get('multi-arch', 'unset') == 'no':
        # Remove explicit "Multi-Arch: no" headers to avoid auto-rejects by dak.
        clear_ma_value = True

    if include_build_id_in_control_file:
        dbgsym_package = binary_package
        if isinstance(binary_package, BinaryPackage):
            dbgsym_package = binary_package.dbgsym_binary_package
        build_ids = parse_dbgsym_build_id_file(dbgsym_package)
        if build_ids:
            dpkg_cmd_line.append('-DBuild-Ids=%s' % " ".join(build_ids))

    if clear_ma_value:
        dpkg_cmd_line.append('-UMulti-Arch')

    return dpkg_cmd_line


@debputy_per_package_task(
    'gencontrol',
    implements_external_command='dh_gencontrol',
    per_package_dependencies=['md5sums', 'installdeb', 'shlibdeps']
)
async def process_package(binary_package: BinaryPackage,
                          build_req: DebputyInvocationContext,
                          parallel_task_limit: asyncio.Semaphore
                          ):
    include_build_id_in_control_file = not binary_package.is_udeb and not binary_package.is_arch_all
    substvars_file = dhemulation.package_ext_file(binary_package,
                                                  'substvars',
                                                  )
    ensure_substvars_present(binary_package, substvars_file)

    if not binary_package.is_arch_all \
            and binary_package.create_or_get_dbgsym_binary_package() and \
            os.path.exists(binary_package.dbgsym_binary_package.staging_dir):
        dbgsym_package = binary_package.dbgsym_binary_package
        dbgsym_cmd_line = _dpkg_cmd(dbgsym_package,
                                    # dpkg-gencontrol uses this to figure out which paragraph to read; the dbgsym is
                                    # not present, but its parent package is, so we use that instead (and then fudge
                                    # everything else)
                                    binary_package.package_name,
                                    substvars_file,
                                    include_build_id_in_control_file=True
                                    )
        await invoke_dpkg_gencontrol(dbgsym_package, dbgsym_cmd_line, parallel_task_limit)
        include_build_id_in_control_file = False

    cmd_line = _dpkg_cmd(binary_package,
                         binary_package.package_name,
                         substvars_file,
                         include_build_id_in_control_file=include_build_id_in_control_file
                         )
    await invoke_dpkg_gencontrol(binary_package, cmd_line, parallel_task_limit)
