import asyncio
import os
import stat

from deb.debputy import dhemulation, cliutil
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.extension import debputy_per_package_task
from deb.debputy.packages import AbstractBinaryPackage


SKIPPED_DEBUG_DIRS = [
    'lib',
    'lib64',
    'usr',
    'bin',
    'sbin',
    'opt',
    'dev',
    'emul',
    '.build-id',
]


async def find_shlibdeps(binary_package: AbstractBinaryPackage, parallel_task_limit: asyncio.Semaphore):
    staging_dir = binary_package.staging_dir
    if not os.path.isdir(staging_dir):
        return

    elf_files_to_process = []
    strip_prefix = len(staging_dir)
    for current_dir, dirs, files in os.walk(staging_dir):
        current_dir_stripped = current_dir[strip_prefix:].lstrip('/')
        if current_dir_stripped == 'usr/lib/debug':
            # Skip the special subdirectories under usr/lib/debug as we do not
            # anticipate finding any real binaries here.
            # - /usr/lib/debug/(lib|lib64|usr|bin|sbin|opt|dev|emul|\.build-id)
            for subdir in SKIPPED_DEBUG_DIRS:
                try:
                    dirs.remove(subdir)
                except ValueError:
                    pass

        for filename in files:
            path = os.path.join(current_dir, filename)
            st_obj = os.lstat(path)
            if not stat.S_ISREG(st_obj.st_mode) or st_obj.st_size < dhemulation.ELF_HEADER_SIZE32:
                continue
            if not dhemulation.is_so_or_exec_elf_file(path, assert_linking_type=dhemulation.ELF_LINKING_TYPE_DYNAMIC):
                continue
            elf_files_to_process.append(path)

    if not elf_files_to_process:
        return

    dhemulation.install_dirs(os.path.join(staging_dir, 'DEBIAN'))
    with dhemulation.substvars_flushed(binary_package) as substvars_file:
        dpkg_cmd = [
            'dpkg-shlibdeps',
            f'-T{substvars_file}'
        ]
        if binary_package.is_udeb:
            dpkg_cmd.append('-tudeb')
        dpkg_cmd.extend(elf_files_to_process)
        await cliutil.run_cmd(*dpkg_cmd, parallel_task_limit=parallel_task_limit)


@debputy_per_package_task(
    'shlibdeps',
    implements_external_command='dh_shlibdeps',
    per_package_dependencies=['makeshlibs']
)
async def process_package(binary_package: AbstractBinaryPackage,
                          build_req: DebputyInvocationContext,
                          parallel_task_limit: asyncio.Semaphore
                          ):
    if binary_package.is_arch_all:
        return
    await find_shlibdeps(binary_package, parallel_task_limit)
