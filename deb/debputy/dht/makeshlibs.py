import asyncio
import os
import re
import stat
import sys
from asyncio import StreamReader
from pathlib import Path
from typing import Optional

from deb.debputy import dhemulation, cliutil
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.extension import debputy_task
from deb.debputy.internal.utils import async_iter
from deb.debputy.packages import BinaryPackage

SONAME_FORMAT_A = re.compile(r'\s+SONAME\s+(.*)\.so\.(.*)')
SONAME_FORMAT_B = re.compile(r'\s+SONAME\s+(.*)-(\d.*)\.so')
SONAME_NON_EMPTY = re.compile(r'\s+SONAME\s+(?:\S)')

REMOVE_DEB_REVISION = re.compile(r'-[^-]$')


async def _all_so_files(binary_package: BinaryPackage, staging_dir):
    if not os.path.isdir(staging_dir):
        return
    objdump = binary_package.cross_command('objdump')
    for current_dir, dirs, files in os.walk(staging_dir):
        for file in files:
            path = os.path.join(current_dir, file)
            st_obj = os.lstat(path)
            if stat.S_ISLNK(st_obj.st_mode) or st_obj.st_size < dhemulation.ELF_HEADER_SIZE32:
                continue
            if not dhemulation.is_so_or_exec_elf_file(path):
                continue
            soname_match = False
            soname = None
            major_version = None
            async with cliutil.cmd_output(objdump, '-p', path) as reader:  # type: StreamReader
                async for line in async_iter(reader.readline, b''):
                    if b'SONAME' not in line:
                        continue
                    line = line.decode('utf-8')
                    match = SONAME_FORMAT_A.search(line)
                    if not match:
                        match = SONAME_FORMAT_B.search(line)

                    if match:
                        soname_match = True
                        soname = match.group(1)
                        major_version = match.group(2)
                        break
                    elif SONAME_NON_EMPTY.search(line):
                        soname_match = True
                        break

            if soname_match:
                continue
            yield path, soname, major_version


async def generate_shlibs_and_call_dpkg_gensymbols(binary_package: BinaryPackage,
                                                   guessed_udeb_package: Optional[BinaryPackage],
                                                   build_req: DebputyInvocationContext,
                                                   parallel_task_limit: asyncio.Semaphore):
    staging_dir = binary_package.staging_dir
    if not os.path.isdir(staging_dir):
        return

    staging_debian_dir = os.path.join(staging_dir, 'DEBIAN')
    installed_shlibs_file = os.path.join(staging_debian_dir, 'shlibs')
    # Remove if it exists
    try:
        os.unlink(installed_shlibs_file)
    except FileNotFoundError:
        pass

    known_udeb_libs = set()
    library_files = []
    shlibs_lines = []
    udeb_shlibs_lines = []
    seen_shlib_lines = set()
    needs_ldconfig = False

    if guessed_udeb_package and 'noudeb' not in build_req.build_environment.deb_build_profiles:
        all_so_files = _all_so_files(guessed_udeb_package, guessed_udeb_package.staging_dir)
        async for libpath, soname, major_version in all_so_files:
            if soname is None or major_version is None:
                continue
            known_udeb_libs.add((soname, major_version))

    source_version = None
    unversioned_library_seen = False

    async for libpath, soname, major_version in _all_so_files(binary_package, staging_dir):
        library_files.append(libpath)
        if soname is None:
            unversioned_library_seen = True
            continue

        if source_version is None:
            source_version = build_req.source_version
            source_version = REMOVE_DEB_REVISION.sub(source_version, '')

        needs_ldconfig = True
        deps = f"{binary_package.package_name} (>= {source_version})"
        shlibs_line = f"{soname} {major_version} {deps}\n"
        if shlibs_line not in seen_shlib_lines:
            seen_shlib_lines.add(shlibs_line)
            shlibs_lines.append(shlibs_line)
            if guessed_udeb_package:
                udeb_deps = f"{guessed_udeb_package.package_name} (>= {source_version})"
                udeb_shlibs_line = f"udeb: {soname} {major_version} {udeb_deps}\n"
                udeb_shlibs_lines.append(udeb_shlibs_line)
                # Track which libraries have been used in the udeb to ensure
                # we spot missing libraries.
                known_udeb_libs.discard((soname, major_version))

    # Sort for determinism
    shlibs_lines.sort()
    udeb_shlibs_lines.sort()
    shlibs_lines.extend(udeb_shlibs_lines)
    del udeb_shlibs_lines

    if guessed_udeb_package and known_udeb_libs:
        # TODO: Improve error handling ... a lot
        sys.exit("udeb SO libraries not found in deb")

    provided_shlibs_file = dhemulation.package_file(binary_package, 'shlibs')
    if provided_shlibs_file:
        dhemulation.install_dirs(staging_debian_dir)
        dhemulation.install_file(provided_shlibs_file, installed_shlibs_file)
    elif shlibs_lines:
        dhemulation.install_dirs(staging_debian_dir)
        with open(installed_shlibs_file, 'w') as fd:
            fd.writelines(shlibs_lines)
        dhemulation.reset_perm(0o644, installed_shlibs_file)

    symbols_file = dhemulation.package_file(binary_package, 'symbols')

    if symbols_file:
        if unversioned_library_seen:
            # There are a few "special" libraries (e.g. nss/nspr)
            # which do not have versioned SONAMES.  However the
            # maintainer provides a symbols file for them and we can
            # then use that to add an ldconfig trigger.
            needs_ldconfig = True

        dpkg_gensymbols_cmd = [
            'dpkg-gensymbols',
            f'-p{binary_package.package_name}',
            f'-I{symbols_file}',
            f'-P{staging_dir}'
        ]
        dpkg_gensymbols_cmd.extend(library_files)

        await cliutil.run_cmd(*dpkg_gensymbols_cmd, parallel_task_limit=parallel_task_limit)

        installed_symbols_file = Path(os.path.join(staging_debian_dir, 'symbols'))

        try:
            st_obj = os.stat(installed_symbols_file)
            if stat.S_ISREG(st_obj.st_mode) and st_obj.st_size > 0:
                dhemulation.reset_perm(0o644, installed_symbols_file)
            else:
                os.unlink(installed_symbols_file)
        except FileNotFoundError:
            pass

    if needs_ldconfig:
        binary_package.autotriggers.add_trigger('activate-noawait', 'ldconfig')


@debputy_task(
    'makeshlibs',
    implements_external_command='dh_makeshlibs',
)
async def process_all_packages(build_req: DebputyInvocationContext, parallel_task_limit: asyncio.Semaphore):
    all_packages = build_req.binary_packages
    all_tasks = []
    loop = asyncio.get_event_loop()
    for binary_package in all_packages:
        if binary_package.is_arch_all or binary_package.is_udeb:
            continue
        guessed_udeb_name = f"{binary_package.package_name}-udeb"
        udeb_package = all_packages.get(guessed_udeb_name)
        if udeb_package is not None and not udeb_package.is_udeb:
            udeb_package = None

        task = generate_shlibs_and_call_dpkg_gensymbols(binary_package,
                                                        udeb_package,
                                                        build_req,
                                                        parallel_task_limit
                                                        )
        all_tasks.append(loop.create_task(task, name=f"makeshlibs/{binary_package.package_name}"))

    await asyncio.gather(*all_tasks)
