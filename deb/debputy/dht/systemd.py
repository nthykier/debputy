import asyncio

from deb.debputy import dhemulation
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.extension import debputy_per_package_task
from deb.debputy.extention_util import install_package_file_if_present, named_after_package, \
    files_directly_in_directory, files_directly_in_directories
from deb.debputy.internal_globals import STANDARD_WARN_ERROR_LOGGER
from deb.debputy.packages import AbstractBinaryPackage


PKG_NAME_DOT_CONF = named_after_package(extension='conf')


@debputy_per_package_task(
    'installsysusers',
    implements_external_command='dh_installsysusers',
    per_package_dependencies=['dh_install']
)
async def process_package_for_sysusers(binary_package: AbstractBinaryPackage,
                                       build_req: DebputyInvocationContext,
                                       parallel_task_limit: asyncio.Semaphore
                                       ):

    typoed_name = dhemulation.package_file(binary_package, 'sysuser')
    if typoed_name:
        STANDARD_WARN_ERROR_LOGGER.warn(f"Possible typo in {typoed_name}: File has been ignored"
                                        " (expect with sysuser*s*)")
    install_package_file_if_present(binary_package, 'sysusers', 'usr/lib/sysusers.d', with_name=PKG_NAME_DOT_CONF)

    had_sysconf = False

    for match in files_directly_in_directory(binary_package, 'usr/lib/sysusers.d',
                                             basename_filter=lambda x: x.endswith('.conf')
                                             ):
        binary_package.maintscript_snippets.add_autoscript(
            'postinst', 'postinst-sysusers', 'debuty-installsysusers',
            variables={'CONFILE_BASENAME': match.basename}
        )
        had_sysconf = True

    if had_sysconf:
        binary_package.substvars.add_dependency('misc:Depends',
                                                'systemd | systemd-standalone-sysusers | systemd-sysusers'
                                                )


@debputy_per_package_task(
    'installtmpfiles',
    implements_external_command='dh_installtmpfiles',
    per_package_dependencies=['dh_install']
)
async def process_package_for_tmpfiles(binary_package: AbstractBinaryPackage,
                                       build_req: DebputyInvocationContext,
                                       parallel_task_limit: asyncio.Semaphore
                                       ):

    typoed_name = dhemulation.package_file(binary_package, 'tmpfile')
    if typoed_name:
        STANDARD_WARN_ERROR_LOGGER.warn(f"Possible typo in {typoed_name}: File has been ignored"
                                        " (expect with tmpfile*s*)")
    install_package_file_if_present(binary_package, 'tmpfiles', 'usr/lib/tmpfiles.d', with_name=PKG_NAME_DOT_CONF)

    tmpfiles = sorted(x.basename
                      for x in files_directly_in_directories(binary_package, ['etc/tmpfiles.d', 'usr/lib/sysusers.d']))
    if tmpfiles:
        binary_package.maintscript_snippets.add_autoscript(
            'postinst', 'postinst-init-tmpfiles', 'debuty-installtmpfiles',
            variables={'TMPFILES': ' '.join(tmpfiles)}
        )
