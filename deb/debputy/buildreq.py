import os
import re
from datetime import datetime
from typing import Iterable

from debian.changelog import Changelog

from deb.debputy.architecture import DpkgArchitectureBuildProcessValuesTable
from deb.debputy.buildenv import BuildEnvironment
from deb.debputy.exceptions import UnsupportedDebhelperCompatLevelError, InvalidDebhelperCompatLevelSpecificationError
from deb.debputy.packages import BinaryPackage, SourcePackage

_MIN_COMPAT_LEVEL = 5
_DH_COMPAT_REGEX = re.compile(r'''
        ^ \s* debhelper-compat \s* [(] \s* = \s* (  # Start capture group for version
                 (?: \d+ : )?                       # Optional epoch
                 [0-9][0-9A-Za-z.+:~]*              # Upstream version with no hyphens
                 (?: - [0-9A-Za-z.+:~]+ )*          # Optional debian revision (+ upstreams versions with hyphens)
        ) \s* [)] \s* $
''', re.VERBOSE)


class PackageTable:

    __slots__ = '_binary_packages'

    def __init__(self, binary_packages: Iterable[BinaryPackage]):
        self._binary_packages = {p.package_name: p for p in binary_packages}

    def __len__(self):
        return len(self._binary_packages)

    def __iter__(self) -> Iterable[BinaryPackage]:
        yield from self._binary_packages.values()

    def __getitem__(self, item: str) -> BinaryPackage:
        return self._binary_packages[item]

    def get(self, k: str) -> BinaryPackage:
        return self._binary_packages.get(k)


class DebputyInvocationContext:

    __slots__ = ['_build_env', '_arch_table', '_source', '_binary_packages', '_source_version', '__changelog_entry',
                 '_source_date_epoch', '_debhelper_compat']

    def __init__(self, build_env: BuildEnvironment, arch_table: 'DpkgArchitectureBuildProcessValuesTable',
                 source: SourcePackage,
                 binary_packages: Iterable[BinaryPackage]):
        self._build_env = build_env
        self._arch_table = arch_table
        self._source = source
        self._binary_packages = PackageTable(binary_packages)
        self._source_version = None
        self.__changelog_entry = None
        self._source_date_epoch = None
        self._debhelper_compat = None

    @property
    def build_environment(self) -> BuildEnvironment:
        return self._build_env

    @property
    def arch_table(self) -> DpkgArchitectureBuildProcessValuesTable:
        return self._arch_table

    @property
    def source(self) -> SourcePackage:
        return self._source

    @property
    def binary_packages(self) -> PackageTable:
        return self._binary_packages

    @property
    def source_date_epoch(self):
        if self._source_date_epoch is None:
            self.ensure_source_date_epoch_is_set()
        return self._source_date_epoch

    def ensure_source_date_epoch_is_set(self):
        seconds = os.environ.get('SOURCE_DATE_EPOCH')
        if seconds is not None:
            self._source_date_epoch = int(seconds)
        else:
            chdate = self._changelog_entry.date
            local_time = datetime.strptime(chdate, "%a, %d %b %Y %H:%M:%S %z")
            self._source_date_epoch = int(local_time.timestamp())
            os.environ['SOURCE_DATE_EPOCH'] = str(self._source_date_epoch)

    @property
    def _changelog_entry(self):
        if self.__changelog_entry is None:
            with open('debian/changelog') as fd:
                self.__changelog_entry = Changelog(fd, max_blocks=1)
        return self.__changelog_entry

    @property
    def source_name(self):
        return self._source.package_name

    @property
    def source_version(self) -> str:
        if self._source_version is None:
            self._source_version = str(self._changelog_entry.version)
        return self._source_version

    @property
    def debhelper_compat(self):
        """Returns the debhelper compat level or -1 if not specified

        Will raise exceptions if the compat level is specified but invalid
        or ambigious.
        """
        if self._debhelper_compat is None:
            self._debhelper_compat = -1
            try:
                compat_env = int(self._build_env.environ['DH_COMPAT'])
            except (KeyError, ValueError):
                compat_env = None

            try:
                with open("debian/compat") as fd:
                    compat_ff = fd.readline().strip()
            except FileNotFoundError:
                compat_ff = None
            else:
                try:
                    compat_ff = int(compat_ff)
                except ValueError:
                    raise UnsupportedDebhelperCompatLevelError(f"The debian/compat file contained {compat_ff}, but the"
                                                               f" should have been a positive integer")
                if compat_ff < _MIN_COMPAT_LEVEL:
                    raise UnsupportedDebhelperCompatLevelError(f"The debian/compat file contained {compat_ff}, but the"
                                                               f" minimum supported compat level is {_MIN_COMPAT_LEVEL}"
                                                               ".")

            compat_bd = None

            for depends in self._source.package_fields.get('Build-Depends', '').split(','):
                if 'debhelper-compat' in depends:
                    match = _DH_COMPAT_REGEX.match(depends)
                    if match is None:
                        depends = depends.strip()
                        raise InvalidDebhelperCompatLevelSpecificationError(f'Found invalid debhelper-compat relation:'
                                                                            f' "{depends}". Please format the relation'
                                                                            f' as (example): debhelper-compat (= 13)')

                    new_compat_bd = int(match.group(1))
                    if compat_bd is not None:
                        raise InvalidDebhelperCompatLevelSpecificationError('Duplicate debhelper-compat'
                                                                            f' build-dependency: {compat_bd} vs.'
                                                                            f' {new_compat_bd}.  The debhelper-compat '
                                                                            'build-dependency must appear at most only'
                                                                            ' once')
                    compat_bd = new_compat_bd

            if compat_bd is not None:
                if compat_bd < _MIN_COMPAT_LEVEL:
                    raise UnsupportedDebhelperCompatLevelError("The debian/control file had a Build-Depends on "
                                                               f"debhelper-compat (= {compat_bd}), but the minimum"
                                                               f" supported compat level is {_MIN_COMPAT_LEVEL}.")
                if compat_ff is not None:
                    raise InvalidDebhelperCompatLevelSpecificationError('Cannot use debian/compat file together with'
                                                                        ' debhelper-compat build-dependency.  Please'
                                                                        ' use exactly one of two methods for specifying'
                                                                        ' the debhelper compatibility level')
                self._debhelper_compat = compat_bd
            elif compat_ff is not None:
                self._debhelper_compat = compat_ff

            if compat_env is not None and self._debhelper_compat != -1:
                self._debhelper_compat = compat_env
        return self._debhelper_compat
