import collections
import os
from typing import Callable, List, Iterable

from deb.debputy import dhemulation
from deb.debputy.packages import AbstractBinaryPackage


def named_after_package(*, extension: str = None):
    def _named_after_package_impl(pkg: AbstractBinaryPackage):
        if extension:
            return f'{pkg.package_name}.{extension}'
        return pkg.package_name

    return _named_after_package_impl


def _as_basename(_, y):
    return y


def _full_name(x, y):
    return os.path.join(x, y)


RETURN_BASENAME = _as_basename
RETURN_FULLNAME = _full_name
FileWithDirectory = collections.namedtuple('FileWithDirectory', ['directory', 'basename', 'path'])

del _as_basename, _full_name


def install_package_file_if_present(package: AbstractBinaryPackage,
                                    package_file: str,
                                    into_directory: str,
                                    *,
                                    with_name: Callable[[AbstractBinaryPackage], str] = None,
                                    mode: int = 0o644,
                                    ):
    pkg_file = dhemulation.package_file(package, package_file)
    if not pkg_file:
        return

    target_name = with_name(package) if with_name is not None else package.package_name
    target_directory = os.path.join(package.staging_dir, into_directory)
    dhemulation.install_dirs(target_directory)
    target_path = os.path.join(target_directory, target_name)
    dhemulation.install_file(pkg_file, target_path, mode=mode)


def files_directly_in_directories(package: AbstractBinaryPackage,
                                  in_directories: List[str],
                                  *,
                                  basename_filter: Callable[[str], bool] = None,
                                  path_filter: Callable[[str], bool] = None,
                                  deduplicate_basename=False,
                                  ) -> Iterable[FileWithDirectory]:

    seen = set() if deduplicate_basename else None

    for directory in in_directories:
        target_directory = os.path.join(package.staging_dir, directory)
        if not os.path.isdir(target_directory):
            continue
        for basename in os.listdir(target_directory):
            if basename_filter is not None and not basename_filter(basename):
                continue

            path = os.path.join(target_directory, basename)
            if not os.path.isfile(path):
                continue

            if deduplicate_basename:
                if basename in seen:
                    continue
                seen.add(basename)

            if path_filter is not None:
                if not path_filter(path):
                    continue

            yield FileWithDirectory(target_directory, basename, path)


def files_directly_in_directory(package: AbstractBinaryPackage,
                                in_directory: str,
                                *,
                                basename_filter: Callable[[str], bool] = None,
                                path_filter: Callable[[str], bool] = None,
                                ) -> Iterable[FileWithDirectory]:
    yield from files_directly_in_directories(package,
                                             [in_directory],
                                             basename_filter=basename_filter,
                                             path_filter=path_filter,
                                             )
