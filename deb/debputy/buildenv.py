import collections.abc
import os
import subprocess

from typing import FrozenSet, Optional, Mapping

from deb.debputy.internal_globals import STANDARD_WARN_ERROR_LOGGER


def _load_dpkg_buildflags_values(env):
    yield from _parse_dpkg_buildflags_output(subprocess.check_output(['dpkg-buildflags'], env=env))


def _parse_dpkg_buildflags_output(output: bytes):
    text = output.decode('utf-8')
    for line in text.splitlines():
        if line[0].isupper():
            k, v = line.strip().split('=', 1)
            yield k, v


class _ReadOnlyDict(collections.abc.Mapping):  # pragma: no cover

    def __init__(self, data):
        self._data = data

    def __getitem__(self, key):
        return self._data[key]

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return iter(self._data)


class BuildEnvironment:
    """Accessor to common environment related values

        >>> env = BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'noudeb nojava'})
        >>> 'noudeb' in env.deb_build_profiles
        True
        >>> 'nojava' in env.deb_build_profiles
        True
        >>> 'nopython' in env.deb_build_profiles
        False
        >>> sorted(env.deb_build_profiles)
        ['nojava', 'noudeb']
    """

    def __init__(self, *, fake_environ=None):
        if fake_environ is None:
            fake_environ = dict(os.environ)
        self.environ = fake_environ
        self._deb_build_profiles = self._parse_set_from_environ('DEB_BUILD_PROFILES')
        self._deb_build_options = self._parse_dict_from_environ('DEB_BUILD_OPTIONS')
        self._rrr = self._parse_rrr()

    @classmethod
    def build_env(cls, base_environment: Mapping[str, str], priority_environment: Mapping[str, str]):
        combined_env = dict(base_environment)
        combined_env.update(priority_environment)
        for k, v in _load_dpkg_buildflags_values(combined_env):
            if k not in priority_environment:
                combined_env[k] = v
        return BuildEnvironment(fake_environ=combined_env)

    def _parse_set_from_environ(self, env_name, default_value=''):
        return frozenset(x for x in self.environ.get(env_name, default_value).split())

    def _parse_dict_from_environ(self, env_name, default_value=''):
        res = {}
        for kvish in self.environ.get(env_name, default_value).split():
            if '=' in kvish:
                key, value = kvish.split('=', 1)
                res[key] = value
            else:
                res[kvish] = None
        return _ReadOnlyDict(res)

    def _parse_rrr(self):
        rrr = frozenset(x for x in self.environ.get('DEB_RULES_REQUIRES_ROOT', 'binary-targets').split())
        if len(rrr) > 1:
            # These only make sense on their own
            rrr = frozenset(rrr - {'no', 'binary-targets'})
            if not rrr:
                # We got DEB_RULES_REQUIRES_ROOT="no binary-targets", which is invalid
                STANDARD_WARN_ERROR_LOGGER.warn('DEB_RULES_REQUIRES_ROOT had invalid value (binary-targets + no). '
                                                'Interpreting it as binary-targets.')
                rrr = frozenset(['binary-targets'])
        return rrr

    @property
    def deb_build_profiles(self) -> FrozenSet[str]:
        """A set-like view of all build profiles active during the build

        >>> env = BuildEnvironment(fake_environ={'DEB_BUILD_PROFILES': 'noudeb nojava'})
        >>> 'noudeb' in env.deb_build_profiles
        True
        >>> 'nojava' in env.deb_build_profiles
        True
        >>> 'nopython' in env.deb_build_profiles
        False
        >>> sorted(env.deb_build_profiles)
        ['nojava', 'noudeb']

        """
        return self._deb_build_profiles

    @property
    def deb_build_options(self) -> Mapping[str, Optional[str]]:
        """A set-like view of all build profiles active during the build

        >>> env = BuildEnvironment(fake_environ={'DEB_BUILD_OPTIONS': 'nostrip parallel=4'})
        >>> 'nostrip' in env.deb_build_options
        True
        >>> 'parallel' in env.deb_build_options
        True
        >>> 'noautodbgsym' in env.deb_build_options
        False
        >>> env.deb_build_options['nostrip'] is None
        True
        >>> env.deb_build_options['parallel']
        '4'
        >>> env.deb_build_options['noautodbgsym']
        Traceback (most recent call last):
            ...
        KeyError: 'noautodbgsym'
        >>> sorted(env.deb_build_options)
        ['nostrip', 'parallel']

        """
        return self._deb_build_options

    def should_use_root(self, *, keyword=None):
        """Check if (fake)root is required (for a given keyword)

        >>> no_root_needed = BuildEnvironment(fake_environ={'DEB_RULES_REQUIRES_ROOT': 'no'})
        >>> no_root_needed.should_use_root()
        False
        >>> root_unconditionally = BuildEnvironment(fake_environ={'DEB_RULES_REQUIRES_ROOT': 'binary-targets'})
        >>> root_unconditionally.should_use_root()
        True
        >>> conditionally_root = BuildEnvironment(fake_environ={'DEB_RULES_REQUIRES_ROOT': 'foo/bar  fu/bar'})
        >>> conditionally_root.should_use_root()
        False
        >>> conditionally_root.should_use_root(keyword="bar/baz")
        False
        >>> conditionally_root.should_use_root(keyword="foo/bar")
        True
        >>> conditionally_root.should_use_root(keyword="fu/bar")
        True

        """
        if self._rrr == {'binary-targets'}:
            return True
        if keyword is None or self._rrr == {'no'}:
            return False
        return keyword in self._rrr
