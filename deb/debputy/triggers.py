from typing import List, Iterable

import typing

from deb.debputy.exceptions import InvalidConfigurationFileInputError, LineReferenceWithPosition

SUPPORTED_TRIGGER_TYPES = {
    'activate': 'await',
    'activate-await': 'await',
    'activate-noawait': 'noawait',

    'interest': 'await',
    'interest-await': 'await',
    'interest-noawait': 'noawait',
}


class TriggerFileContent:

    def serialize(self) -> Iterable[str]:
        raise NotImplementedError()


class CommentOrEmptyLine(TriggerFileContent):

    __slots__ = ['_comment']

    def __init__(self, comment):
        self._comment = comment
        if comment and (comment[0] != '#' and comment.strip()):
            raise ValueError("Comment lines must start with a # sign")

    def serialize(self) -> Iterable[str]:
        yield self._comment


class Trigger(TriggerFileContent):

    __slots__ = ['_trigger_type', '_trigger_name']

    def __init__(self, trigger_type, trigger_name):
        self._trigger_type = trigger_type
        self._trigger_name = trigger_name
        if trigger_type not in SUPPORTED_TRIGGER_TYPES:  # pragma: no cover
            raise ValueError(f"trigger_type must be one of {sorted(SUPPORTED_TRIGGER_TYPES)}, got {trigger_type}")

    def __hash__(self):
        return hash((self._trigger_type, self._trigger_name))

    def __eq__(self, other):
        if other is None or not isinstance(other, Trigger):
            return False
        other_cast = typing.cast('Trigger', other)
        return self._trigger_type == other_cast._trigger_type and self._trigger_name == other_cast._trigger_name

    @property
    def trigger_type(self):
        return self._trigger_type

    @property
    def trigger_name(self):
        return self._trigger_name

    def serialize(self) -> Iterable[str]:
        yield f"{self._trigger_type} {self._trigger_name}"


class DebputyTriggerBundle(TriggerFileContent):

    __slots__ = ['_triggers']

    def __init__(self):
        self._triggers: List[Trigger] = []

    def __bool__(self):
        return bool(self._triggers)

    def append(self, trigger):
        self._triggers.append(trigger)

    def serialize(self) -> Iterable[str]:
        if self._triggers:
            yield "# Triggers added by debputy"
            for t in self._triggers:
                yield from t.serialize()


class Triggers:
    """Triggers is a Round-trip safe parser of dpkg trigger files

    Example of its usage:
    >>> import io
    >>> triggers = Triggers()
    >>> content = '''
    ... # Some comment
    ... activate-noawait foo
    ...
    ... # Triggers added by debputy
    ... activate-noawait ldconfig
    ... # another comment
    ... '''
    >>> triggers.read_triggers(io.StringIO(content), name="some-file")
    >>> triggers.empty
    False
    >>> triggers.add_trigger('activate-noawait', 'blubber')
    >>> output = io.StringIO()
    >>> triggers.write_triggers(output)
    >>> result = '''
    ... # Some comment
    ... activate-noawait foo
    ...
    ... # Triggers added by debputy
    ... activate-noawait ldconfig
    ... activate-noawait blubber
    ... # another comment
    ... '''
    >>> output.getvalue() == result
    True
    >>> # If it debputy has not added triggers before, it appends them in the bottom
    >>> triggers.read_triggers(io.StringIO(content.replace('debputy', 'something else')), name="some-file")
    >>> # this will not be re-added (as it was already present)
    >>> triggers.add_trigger('activate-noawait', 'ldconfig')
    >>> triggers.add_trigger('activate-noawait', 'blubber')
    >>> result = '''
    ... # Some comment
    ... activate-noawait foo
    ...
    ... # Triggers added by something else
    ... activate-noawait ldconfig
    ... # another comment
    ... # Triggers added by debputy
    ... activate-noawait blubber
    ... '''
    >>> output2 = io.StringIO()
    >>> triggers.write_triggers(output2)
    >>> output2.getvalue() == result
    True

    """

    __slots__ = ['_debputy_bundle', '_triggers', '_seen_triggers']

    def __init__(self):
        self._debputy_bundle = DebputyTriggerBundle()
        self._triggers: List[TriggerFileContent] = [self._debputy_bundle]
        self._seen_triggers = set()

    def add_trigger(self, trigger_type: str, trigger_name: str):
        """Add a dependency clause to a given substvar

        >>> triggers = Triggers()
        >>> triggers.add_trigger('activate-noawait', 'ldconfig')
        >>> triggers.add_trigger('interest-noawait', '/some/path/else/where')
        >>> # Exact duplicates are automatically de-duplicated
        >>> triggers.add_trigger('activate-noawait', 'ldconfig')

        """
        trigger = Trigger(trigger_type, trigger_name)
        if trigger in self._seen_triggers:
            return

        self._debputy_bundle.append(trigger)
        self._seen_triggers.add(trigger)

    @property
    def empty(self):
        """Tells whether there are any content

        >>> triggers = Triggers()
        >>> triggers.empty
        True
        >>> triggers.add_trigger('activate-noawait', 'ldconfig')
        >>> triggers.empty
        False
        >>> # Note that content can be "comment-only".
        >>> import io
        >>> content = "# some comment"
        >>> triggers = Triggers()
        >>> triggers.empty
        True
        >>> triggers.read_triggers(io.StringIO(content), name="some-file")
        >>> triggers.empty
        False
        >>> # Reading an empty file will of course mean an empty trigger object
        >>> triggers.read_triggers(io.StringIO(''), name="some-file")
        >>> triggers.empty
        True

        """
        return True if len(self._triggers) <= 1 and not self._debputy_bundle else False

    def write_triggers(self, fd):
        for serializable in self._triggers:
            fd.writelines("{}\n".format(line)
                          for line in serializable.serialize()
                          )

    def read_triggers(self, fd, *, name=None):
        """Read triggers from a file in the format supported by dpkg

        All existing content will be discarded and only triggers from the
        file will be present after this methods completes.  In case of an
        IOError, the object will be in an undefined state.

        :param fd: An iterable of lines to read
        :param name: Name to use for the file descriptor (defaults to fd.name)
        """

        if name is None:
            name = fd.name

        # Reset
        self._seen_triggers.clear()
        self._debputy_bundle = DebputyTriggerBundle()
        self._triggers = []

        parsing_debputy_triggers = None

        for lineno, line in enumerate(fd, start=1):
            line = line.rstrip('\n')
            if line.strip() == '' or line[0] == '#':
                if line == "# Triggers added by debputy" and parsing_debputy_triggers is None:
                    parsing_debputy_triggers = True
                    self._triggers.append(self._debputy_bundle)
                    continue
                if parsing_debputy_triggers:
                    parsing_debputy_triggers = False
                self._triggers.append(CommentOrEmptyLine(line))
                continue
            parts = line.split(' ', 1)
            if len(parts) != 2:
                if len(parts) > 2:
                    error_message = "Unsupported extra text after trigger_name"
                    loc_ref = LineReferenceWithPosition.loc_ref_from_word_no_until_eol(name, lineno, line, 3)
                else:
                    error_message = "Missing trigger_name (2nd word) on the line"
                    loc_ref = LineReferenceWithPosition.loc_ref_entire_line(name, lineno, line)

                raise InvalidConfigurationFileInputError(error_message, loc_ref)

            trigger_type, trigger_name = parts
            trigger = Trigger(trigger_type, trigger_name)
            self._seen_triggers.add(trigger)
            if parsing_debputy_triggers:
                self._debputy_bundle.append(trigger)
            else:
                self._triggers.append(trigger)
        if parsing_debputy_triggers is None:
            self._triggers.append(self._debputy_bundle)
