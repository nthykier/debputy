from typing import TypeVar, Callable, Coroutine, Any, AsyncIterable


T = TypeVar('T')


async def async_iter(func: Callable[[], Coroutine[Any, Any, T]], sentinel: T) -> AsyncIterable[T]:
    while True:
        value = await func()
        if value == sentinel:
            break
        yield value
