import logging


DEBUG_LOGGER = logging.getLogger('deb.debputy.DEBUG_LOGGER')
STANDARD_OUTPUT_LOGGER = logging.getLogger('deb.debputy.STANDARD_OUTPUT_LOGGER')
STANDARD_WARN_ERROR_LOGGER = logging.getLogger('deb.debputy.STANDARD_WARN_ERROR_LOGGER')
DEFAULT_PACKAGE_TYPE = 'deb'
DBGSYM_PACKAGE_TYPE = 'deb'
UDEB_PACKAGE_TYPE = 'udeb'
