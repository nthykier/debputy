import os
import shutil
import typing
from typing import Dict, Set, Optional, Union

from deb.debputy.internal_globals import DEBUG_LOGGER


class Substvar:

    __slots__ = ['_value']

    def __init__(self, initial_value: Optional[Union[str, Set[str]]] = ""):
        # We have 3 values for _value:
        # 1) None: The variable in unset
        # 2) string: The variable is set to a fixed string.  This variant is
        #    is round-trip safe.
        # 3) set: The variable is dependency-like field.  This variant is *NOT*
        #    round-trip safe.
        #
        # When reading substvars from files, we always use variant 2) and then
        # lazily convert to 3) when necessary.  This choice makes the substvars
        # round-trip safe by default until someone messes with a substvar.
        self._value: Optional[Union[str, Set[str]]] = initial_value

    def add_dependency(self, dependency_clause):
        if self._value == "":
            self._value = {dependency_clause}
            return
        if isinstance(self._value, str):
            # Convert to dependency format
            self._value = self._as_dependency_set()
        self._value.add(dependency_clause)

    def _as_dependency_set(self) -> Set[str]:
        if isinstance(self._value, set):
            return self._value
        return {v.strip() for v in self._value.split(',')}

    def resolve(self):
        if isinstance(self._value, set):
            return ", ".join(sorted(self._value))
        return self._value

    def __eq__(self, other):
        if other is None or not isinstance(other, Substvar):
            return False
        other_as_subst = typing.cast('Substvar', other)
        if self._value == other_as_subst._value:
            return True
        return self._as_dependency_set() == other_as_subst._as_dependency_set()


class Substvars:
    """Substvars is a dict-like object containing known substvars for a given package.

    >>> substvars = Substvars()
    >>> substvars['foo'] = 'bar, golf'
    >>> substvars['foo']
    'bar, golf'
    >>> substvars.add_dependency('foo', 'dpkg (>= 1.20.0)')
    >>> substvars['foo']
    'bar, dpkg (>= 1.20.0), golf'
    >>> 'foo' in substvars
    True
    >>> sorted(substvars)
    ['foo']
    >>> del substvars['foo']
    >>> substvars['foo']
    Traceback (most recent call last):
        ...
    KeyError: 'foo'
    >>> substvars.get('foo')
    >>> # None
    >>> substvars['foo'] = ""
    >>> substvars['foo']
    ''

    The Substvars object also provide methods for serializing and deserializing
    the substvars into and from the format used by dpkg-gencontrol.
    """

    __slots__ = ['_vars_dict']

    def __init__(self):
        self._vars_dict: Dict[str, Substvar] = {}

    @property
    def _vars(self) -> Dict[str, Substvar]:
        return self._vars_dict

    def add_dependency(self, substvar, dependency_clause):
        """Add a dependency clause to a given substvar

        >>> substvars = Substvars()
        >>> # add_dependency automatically creates variables
        >>> 'misc:Recommends' not in substvars
        True
        >>> substvars.add_dependency('misc:Recommends', "foo (>= 1.0)")
        >>> substvars['misc:Recommends']
        'foo (>= 1.0)'
        >>> # It can be append to other variables
        >>> substvars['foo'] = 'bar, golf'
        >>> substvars.add_dependency('foo', 'dpkg (>= 1.20.0)')
        >>> substvars['foo']
        'bar, dpkg (>= 1.20.0), golf'
        >>> # Exact duplicates are ignored
        >>> substvars.add_dependency('foo', 'dpkg (>= 1.20.0)')
        >>> substvars['foo']
        'bar, dpkg (>= 1.20.0), golf'

        """
        try:
            variable = self._vars[substvar]
        except KeyError:
            variable = Substvar()
            self._vars[substvar] = variable
        variable.add_dependency(dependency_clause)

    def get(self, key):
        return self._vars.get(key)

    def __iter__(self):
        return iter(self._vars)

    def __contains__(self, item):
        return item in self._vars

    def __getitem__(self, key):
        return self._vars[key].resolve()

    def __delitem__(self, key):
        del self._vars[key]

    def __setitem__(self, key, value):
        self._vars[key] = Substvar(value)

    def __eq__(self, other):
        if other is None or not isinstance(other, Substvars):
            return False
        other_as_substvars = typing.cast('Substvars', other)
        return self._vars == other_as_substvars._vars

    def write_substvars(self, path):
        with open(path, 'w', encoding='utf-8') as fd:
            fd.writelines("{}={}\n".format(k, v.resolve())
                          for k, v in self._vars.items()
                          )

    def read_substvars(self, path):
        """Read substvars from a file in the format supported by dpkg-gencontrol

        All existing variables will be discarded and only variables from the
        file will be present after this methods completes.  In case of an
        IOError (other than FileNotFoundError or other "failed to open the file
        errors"), the object will be in an undefined state.

        If the file denoted by path cannot be read (i.e. open fails), then this
        method fails before altering the state of the object.

        :param path: The Path-like object to read from
        """
        with open(path, 'r', encoding='utf-8') as fd:
            self._vars_dict.clear()
            for line in fd:
                if line.strip() == '' or line[0] == '#':
                    continue
                parts = line.rstrip("\r\n").split('=', 1)
                if len(parts) == 1:
                    varname = parts[0]
                    value = ""
                else:
                    varname, value = parts
                self._vars_dict[varname] = Substvar(value)


class FlushableSubstvars(Substvars):

    __slots__ = ['_flushed_location', '_flush_active']

    def __init__(self):
        super().__init__()
        self._flushed_location = None
        self._flush_active = False

    @property
    def _vars(self) -> Dict[str, Substvar]:
        if self._flush_active:
            raise RuntimeError("Access to substvar while it is flushed")
        if self._flushed_location is not None:
            DEBUG_LOGGER.debug("[DEBUG] [Substvars] Loading flushed substvars %s due to access", self._flushed_location)
            try:
                self.read_substvars(self._flushed_location)
            except FileNotFoundError:
                # Deleted => no substvars
                self._vars_dict.clear()
            else:
                os.unlink(self._flushed_location)
            self._flushed_location = None
        return self._vars_dict

    def initially_flushed(self, path):
        if self._flush_active or self._vars_dict:
            raise RuntimeError("Cannot initialize to flushed; substvars already initialized or flushed")
        self._flushed_location = path
        self._flush_active = False

    def flush_finished(self):
        if not self._flush_active:
            raise RuntimeError("flush_finished called but substvars were not flushed")
        self._flush_active = False

    def flush_substvars(self, path: str):
        if self._flush_active:
            raise RuntimeError("Cannot flush substvars twice")
        if self._flushed_location is not None:
            # If the file is currently flushed, then just rename the file to match
            if self._flushed_location != path and not os.path.samefile(self._flushed_location, path):
                shutil.move(self._flushed_location, path)
        else:
            self.write_substvars(path)
        self._flushed_location = path
        self._flush_active = True
