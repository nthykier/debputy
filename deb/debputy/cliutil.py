import asyncio
import contextlib
import io
import logging
import os
import re
import signal
from asyncio import StreamReader
from typing import Optional, AsyncIterator

from .internal_globals import STANDARD_OUTPUT_LOGGER


EMPTY_DICT = {}


class CommandFailedError(RuntimeError):

    @property
    def raw_return_code(self):
        return self.args[0]

    @property
    def command_line(self):
        return self.args[1]

    @property
    def _has_kwargs(self):
        return len(self.args) > 2

    @property
    def _kwargs(self):
        return self.args[2] if self._has_kwargs else EMPTY_DICT

    def format_command(self):
        return format_command(*self.command_line, **self._kwargs)

    def pretty_format_error(self):
        raw_code = self.raw_return_code
        command = self.format_command()
        if raw_code < 0:
            signal_no = abs(raw_code)
            try:
                signal_name = "%s (%d)" % (signal.Signals(signal_no).name, signal_no)
            except ValueError:
                signal_name = signal_no
            return "%s died with signal %s" % (command, signal_name)
        return "%s returned exit code %s" % (command, raw_code)


try:
    SC_ARG_MAX = os.sysconf('SC_ARG_MAX')
except ValueError:
    SC_ARG_MAX = -1
if SC_ARG_MAX < 20000:  # Limit used by debhelper
    SC_ARG_MAX = 20000

XARGS_INSERT_PARAMS_HERE = object()
RE_SPACE = re.compile(r'\s')
RE_ESCAPE_IN_DQUOTE = re.compile(r'([\n`$"\\])')
# This list is from _Unix in a Nutshell_. (except '#')
RE_ESCAPE_NO_QUOTE = re.compile(r'([\s!"$()*+#;<>?@\[\]\\`|~])')

RE_SUB_BACKSLASH_MATCH = r'\\' + r'\1'


# Transliteration of escape_shell from debhelper
def _escape_shell(*args):
    for arg in args:
        if RE_SPACE.search(arg):
            # Escape only a few things since it will be quoted.
            # Note we use double quotes because you cannot
            # escape ' in single quotes, while " can be escaped
            # in double.
            # This does make -V"foo bar" turn into "-Vfoo bar",
            # but that will be parsed identically by the shell
            # anyway..
            escaped = RE_ESCAPE_IN_DQUOTE.sub(RE_SUB_BACKSLASH_MATCH, arg)
            # Add double quotes
            yield '"%s"' % escaped
        else:
            yield RE_ESCAPE_NO_QUOTE.sub(RE_SUB_BACKSLASH_MATCH, arg)


def format_shell_command(shell_cmd, cwd=None):
    prefix_parts = []
    inject_and_signs = False
    if cwd is not None:
        prefix_parts.append('cd %s' % " ".join(_escape_shell(cwd)))
        inject_and_signs = True
    if inject_and_signs:
        prefix_parts.append('&&')
        prefix_parts.append(shell_cmd)
        final_cmd = " ".join(prefix_parts)
    else:
        final_cmd = shell_cmd

    # Intend 3 spaces
    return "   " + final_cmd


def format_command(*cmd_line, cwd=None):
    prefix_parts = []
    inject_and_signs = False
    if cwd is not None:
        prefix_parts.append('cd %s' % " ".join(_escape_shell(cwd)))
        inject_and_signs = True
    if inject_and_signs:
        prefix_parts.append('&&')
        prefix_parts.extend(_escape_shell(*cmd_line))
        final_cmd = prefix_parts
    else:
        final_cmd = _escape_shell(*cmd_line)

    # Intend 3 spaces
    return "   " + " ".join(final_cmd)


def log_command(*cmd_line, **kwargs):
    if not STANDARD_OUTPUT_LOGGER.isEnabledFor(logging.INFO):
        return
    STANDARD_OUTPUT_LOGGER.info(format_command(*cmd_line, **kwargs))


def log_shell_command(shell_cmd, **kwargs):
    if not STANDARD_OUTPUT_LOGGER.isEnabledFor(logging.INFO):
        return
    STANDARD_OUTPUT_LOGGER.info(format_shell_command(shell_cmd, **kwargs))


async def run_cmd(*cmd_line, parallel_task_limit: asyncio.Semaphore = None, **kwargs):
    async with _parallel_limit(parallel_task_limit):
        log_command(*cmd_line, **kwargs)
        proc = await asyncio.create_subprocess_exec(*cmd_line, **kwargs)
        res = await proc.wait()
    if res != 0:
        raise CommandFailedError(res, cmd_line, kwargs)


@contextlib.asynccontextmanager
async def cmd_output(*cmd_line, parallel_task_limit: asyncio.Semaphore = None,
                     cwd=None, environ=None
                     ) -> AsyncIterator[StreamReader]:
    """Run a command and process its output asynchronously

    Usage:
        with cmd_output(['/bin/ls']) as stdout:
            while True:
                line = await stdout.readline()
                if line == b'':
                    break
                # ...

    (Alternative for internal use only)
        with cmd_output(['/bin/ls']) as stdout:
            async for line in async_iter(stdout.readline):
                # ...


    CAVEAT: If parallel_task_limit is used, the Semaphore counter is reduced
      for the entire duration of the with-block.  This can cause deadlocks
      if the body of the with-block also want to consume a counter.

      Handling that corner-case is left as an exercise to the caller.

    """
    async with _parallel_limit(parallel_task_limit):
        proc = await asyncio.create_subprocess_exec(*cmd_line,
                                                    stdin=asyncio.subprocess.DEVNULL,
                                                    stdout=asyncio.subprocess.PIPE,
                                                    cwd=cwd,
                                                    environ=environ,
                                                    )
        yield proc.stdout

        if not proc.stdout.at_eof():
            # Drain
            while True:
                r = await proc.stdout.read(io.DEFAULT_BUFFER_SIZE)
                if r == b'':
                    break

        res = await proc.wait()

    if res != 0:
        raise CommandFailedError(res, cmd_line, {cwd: cwd})


@contextlib.asynccontextmanager
async def _parallel_limit(parallel_task_limit: Optional[asyncio.Semaphore]):
    if parallel_task_limit is None:
        yield
    else:
        async with parallel_task_limit:
            yield


def _split_xargs_args(dynamic_len, args):
    current_len = 0
    selected = []
    for arg in args:
        # +1 for the delimiting space
        arg_len = len(arg) + 1
        current_len += arg_len
        if current_len >= dynamic_len:
            current_len = arg_len
            assert current_len < dynamic_len, \
                f"Static arguments is too long even with using only argument {arg}.  Please limit the command length"
            yield selected
            selected.clear()
        selected.append(arg)
    if selected:
        yield selected


async def xargs(static_args, args, *, max_cmd_len=None, parallel_task_limit: asyncio.Semaphore = None):
    if max_cmd_len is None:
        max_cmd_len = SC_ARG_MAX

    try:
        subst_index = static_args.index(XARGS_INSERT_PARAMS_HERE)
        if static_args.count(XARGS_INSERT_PARAMS_HERE) > 1:
            raise ValueError("XARGS_INSERT_PARAMS_HERE can be used at most once!")
    except ValueError:
        subst_index = -1

    if subst_index == len(static_args) - 1:
        # Special-case: if XARGS_INSERT_PARAMS_HERE is the last argument, then
        # rewrite the input as if it had been absent.  This makes the code use
        # the simpler (slightly faster) flow and simplifies bounds logic when
        # inserting the dynamic args
        subst_index = -1
        static_args.pop()

    # Sanity check of subst_index
    assert subst_index != 0, "XARGS_INSERT_PARAMS_HERE as command!?"

    static_len = sum(len(x) if x is not XARGS_INSERT_PARAMS_HERE else 0 for x in static_args) + len(static_args) + 1
    dynamic_len = max_cmd_len - static_len
    if dynamic_len < 1:
        raise ValueError("Static arguments take up too many characters (len %d, max permitted %s)" % (
            static_len, max_cmd_len))

    for xarg_payload in _split_xargs_args(dynamic_len, args):
        if subst_index == -1:
            cmd_line = static_args.copy()
            cmd_line.extend(xarg_payload)
        else:
            cmd_line = static_args[:subst_index]
            cmd_line.extend(xarg_payload)
            cmd_line.extend(static_args[subst_index + 1:])

        await run_cmd(*cmd_line, parallel_task_limit=parallel_task_limit)
