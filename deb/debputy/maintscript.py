import os
import string
from typing import Dict, List

from deb.debputy import dhemulation
from deb.debputy.exceptions import AutoscriptSnippetNotFoundError
from deb.debputy.internal_globals import STANDARD_OUTPUT_LOGGER

SUPPORTED_SCRIPT_NAMES = [
    ('postinst', False),
    ('postrm', True),
    ('preinst', False),
    ('prerm', True),
]


PROVIDER_WRAPPING = """\
# Automatically added by {provider_tag}
{value}
# End automatically added section
"""


DATAFILE_SEARCH_PATH = []
DH_DATAFILES = os.environ.get('DH_DATAFILES')
DH_AUTOSCRIPTS = os.environ.get('DH_AUTOSCRIPTS')

if DH_DATAFILES is not None:
    DATAFILE_SEARCH_PATH.extend(DH_DATAFILES.split(':'))
DATAFILE_SEARCH_PATH.append('/usr/share/debhelper')


def _autoscript_locations(snippet_name: str):
    if DH_AUTOSCRIPTS is not None:
        yield os.path.join(DH_AUTOSCRIPTS, snippet_name)
    yield from (os.path.join(p, 'autoscripts', snippet_name) for p in DATAFILE_SEARCH_PATH)


def default_maintscript_lookup(snippet_name: str):
    for snippet_path in _autoscript_locations(snippet_name):
        if os.path.isfile(snippet_path):
            with open(snippet_path) as fd:
                return fd.read()
    raise AutoscriptSnippetNotFoundError(
        f"Could not locate autoscript snippet {snippet_name}",
        snippet_name,
        list(_autoscript_locations(snippet_name))
    )


class MaintscriptTemplate(string.Template):

    pattern = r'[#](?P<named>(?a:[_a-zA-Z][_a-zA-Z0-9]*))[#]'


VALID_SNIPPET_TYPES = [
    'default',
    'service',
]


class MaintscriptSnippet:

    __slots__ = ['_removal', '_parts']

    def __init__(self, removal=False):
        self._removal: bool = removal
        self._parts: Dict[str, List[str]] = {}

    def append_raw(self, content: str, *, snippet_type: str = None):
        if snippet_type is None:
            snippet_type = VALID_SNIPPET_TYPES[0]
        try:
            self._parts[snippet_type].append(content)
        except KeyError:
            if snippet_type not in VALID_SNIPPET_TYPES:
                raise
            self._parts[snippet_type] = [content]

    def resolve(self) -> str:
        if not self._parts:
            return ""

        order = reversed(VALID_SNIPPET_TYPES) if self._removal else VALID_SNIPPET_TYPES
        snippet_value_parts = []
        for snippet_type in order:
            try:
                snippet_value = self._parts[snippet_type]
            except KeyError:
                continue

            if not snippet_value:
                del self._parts[snippet_type]
                continue

            if len(snippet_value) > 1:
                parts = reversed(snippet_value) if self._removal else snippet_value
                combined = "".join(parts)
                snippet_value.clear()
                snippet_value.append(combined)
            snippet_value_parts.append(snippet_value[0])

        return "".join(snippet_value_parts)


class MaintscriptSnippets:

    def __init__(self, snippet_lookup):
        self._sippets: Dict[str, MaintscriptSnippet] = {
            name: MaintscriptSnippet(is_removal)
            for name, is_removal in SUPPORTED_SCRIPT_NAMES
        }
        self._snippet_lookup = snippet_lookup

    def add_autoscript(self,
                       script_name: str,
                       snippet_name: str,
                       provider_tag: str,
                       *,
                       variables: Dict[str, str] = None,
                       ):
        contents_raw = self._snippet_lookup(snippet_name)
        if variables:
            template = MaintscriptTemplate(contents_raw)
            value = template.safe_substitute(variables)
        else:
            value = contents_raw

        # Prune trailing newline; looks better in the generated script
        value = value.rstrip('\r\n')
        value = PROVIDER_WRAPPING.format(provider_tag=provider_tag, value=value)
        self._sippets[script_name].append_raw(value)

    def load_previous_generated_script(self, binary_package):
        for script_name in self._sippets:
            snippet = self._sippets[script_name]
            assert snippet.resolve() == ""

            for snippet_type in VALID_SNIPPET_TYPES:
                if snippet_type == 'default':
                    script_path = dhemulation.package_ext_file(binary_package, f"{script_name}.debhelper")
                else:
                    script_path = dhemulation.generated_file(binary_package,
                                                             f"{script_name}.{snippet_type}",
                                                             create_dirs=False
                                                             )

                if not os.path.isfile(script_path):
                    continue

                STANDARD_OUTPUT_LOGGER.info("Loading existing generated %s script %s (%s) for package %s",
                                            script_path,
                                            script_name,
                                            snippet_type,
                                            binary_package.package_name
                                            )

                with open(script_path) as fd:
                    content = fd.read()
                snippet.append_raw(content, snippet_type=snippet_type)

    def __iter__(self):
        return iter(self._sippets)

    def __getitem__(self, item):
        return self._sippets[item].resolve()
