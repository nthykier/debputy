import os
import re
from typing import Dict, Optional, Union

import typing
from debian.deb822 import Deb822

from deb.debputy.architecture import DpkgArchitectureBuildProcessValuesTable, debarch_is
from deb.debputy.buildenv import BuildEnvironment
from deb.debputy.exceptions import MissingMandatoryFieldInDebianControlFileError
from deb.debputy.internal_globals import UDEB_PACKAGE_TYPE, STANDARD_WARN_ERROR_LOGGER, DBGSYM_PACKAGE_TYPE, \
    DEFAULT_PACKAGE_TYPE
from deb.debputy.maintscript import MaintscriptSnippets, default_maintscript_lookup
from deb.debputy.substvar import Substvars, FlushableSubstvars
from deb.debputy.triggers import Triggers


PROFILE_GROUP_SPLIT = re.compile(r'>\s+<')
MANDATORY_BINARY_PACKAGE_FIELD = [
    'Package',
    'Architecture',
]


def _enabled_via_profiles(package_name: str, profiles_raw: str, build_env: BuildEnvironment):
    profiles_raw = profiles_raw.strip()
    if profiles_raw[0] != '<' or profiles_raw[-1] != '>' or profiles_raw == '<>':
        STANDARD_WARN_ERROR_LOGGER.warn(f"Could not parse Build-Profiles field for {package_name}: Ignoring it")
        return []
    profile_groups = PROFILE_GROUP_SPLIT.split(profiles_raw[1:-1])
    active_build_profiles = build_env.deb_build_profiles
    for profile_group_raw in profile_groups:
        should_process_package = True
        for profile_name in profile_group_raw.split():
            negation = False
            if profile_name[0] == '!':
                negation = True
                profile_name = profile_name[1:]

            matched_profile = profile_name in active_build_profiles
            if matched_profile == negation:
                should_process_package = False
                break

        if should_process_package:
            return True

    return False


def create_binary_package(paragraph: Union[Deb822, Dict[str, str]],
                          selected_packages: typing.Set[str],
                          excluded_packages: typing.Set[str],
                          select_arch_all: bool,
                          select_arch_any: bool,
                          arch_table: DpkgArchitectureBuildProcessValuesTable,
                          build_env: BuildEnvironment,
                          paragraph_index: int,
                          ) -> 'BinaryPackage':

    try:
        package_name = paragraph['Package']
    except KeyError as e:
        raise MissingMandatoryFieldInDebianControlFileError(
            f'Missing mandatory field "Package" in paragraph number {paragraph_index}',
            "Package",
            paragraph_index,
            "<N/A>"
        ) from e

    for mandatory_field in MANDATORY_BINARY_PACKAGE_FIELD:
        if mandatory_field not in paragraph:
            raise MissingMandatoryFieldInDebianControlFileError(
                f'Missing mandatory field "{mandatory_field}" for '
                f'binary package {package_name} (paragraph number {paragraph_index})',
                mandatory_field,
                paragraph_index,
                package_name
            )

    architecture = paragraph['Architecture']

    if paragraph_index < 1:
        raise ValueError("paragraph index must be 1-indexed (1, 2, ...)")
    is_main_package = paragraph_index == 1

    if package_name in excluded_packages:
        should_act_on = False
    elif package_name in selected_packages:
        should_act_on = True
    elif architecture == "all":
        should_act_on = select_arch_all
    else:
        should_act_on = select_arch_any

    profiles_raw = paragraph.get('Build-Profiles', '').strip()
    if should_act_on and profiles_raw:
        should_act_on = _enabled_via_profiles(package_name, profiles_raw, build_env)

    return BinaryPackage(paragraph,
                         arch_table,
                         should_be_acted_on=should_act_on,
                         is_main_package=is_main_package)


class AbstractBinaryPackage:

    @property
    def should_be_acted_on(self) -> bool:  # pragma: no cover
        raise NotImplementedError()

    @property
    def package_name(self):
        try:
            return self.package_fields['Package']
        except KeyError:
            STANDARD_WARN_ERROR_LOGGER.error("Paragraph in debian/control is missing mandatory Package field")
            raise

    @property
    def package_type(self):  # pragma: no cover
        raise NotImplementedError()

    @property
    def package_fields(self) -> Dict[str, str]:  # pragma: no cover
        raise NotImplementedError()

    @property
    def declared_architecture(self):
        try:
            return self.package_fields['Architecture']
        except KeyError:
            STANDARD_WARN_ERROR_LOGGER.error("Paragraph in debian/control is missing mandatory Architecture field")
            raise

    @property
    def binary_architecture(self):
        raise NotImplementedError()  # pragma: no cover

    @property
    def is_arch_all(self) -> bool:
        return self.declared_architecture == 'all'

    @property
    def archive_section(self) -> str:
        value = self.package_fields.get('Section')
        if value is None:
            STANDARD_WARN_ERROR_LOGGER.warn("Package %s does not have a Section field" % self.package_name)
            return 'Unknown'
        return value

    @property
    def archive_component(self) -> str:
        component = ''
        section = self.archive_section
        if '/' in section:
            component = section.rsplit('/', 1)[0]
            # The "main" component is always shortened to ""
            if component == 'main':
                component = ''
        return component

    @property
    def is_udeb(self) -> bool:
        return self.package_type == UDEB_PACKAGE_TYPE

    @property
    def staging_dir(self):
        return 'debian/%s' % self.package_name

    @property
    def is_main_package(self) -> bool:
        return False

    @property
    def has_dbgsym_binary_package(self) -> bool:
        return False

    @property
    def dbgsym_binary_package(self) -> 'Optional[DbgsymBinaryPackage]':  # pragma: no cover
        raise TypeError("{} cannot provide a DbgsymBinaryPackage".format(self.__class__.__name__))

    @property
    def substvars(self) -> FlushableSubstvars:  # pragma: no cover
        raise NotImplementedError()

    @property
    def maintscript_snippets(self) -> MaintscriptSnippets:  # pragma: no cover
        raise NotImplementedError()

    @property
    def autotriggers(self) -> Triggers:  # pragma: no cover
        raise NotImplementedError()


class DbgsymBinaryPackage(AbstractBinaryPackage):

    __slots__ = ['parent_package']

    def __init__(self, parent_package: 'BinaryPackage'):
        super(DbgsymBinaryPackage, self).__init__()
        self.parent_package = parent_package

    @property
    def should_be_acted_on(self) -> bool:
        return self.parent_package.should_be_acted_on

    @property
    def package_name(self):
        return "%s-dbgsym" % self.parent_package.package_name

    @property
    def binary_architecture(self):
        return self.parent_package.binary_architecture

    @property
    def package_type(self):
        return DBGSYM_PACKAGE_TYPE

    @property
    def package_fields(self) -> Dict[str, str]:
        return self.parent_package.package_fields

    @property
    def archive_section(self) -> str:
        value = 'debug'
        component = self.archive_component
        if component != '':
            return "%s/%s" % (component, value)
        return value

    @property
    def archive_component(self) -> str:
        return self.parent_package.archive_component

    @property
    def dbgsym_info_dir(self):
        return os.path.join('debian', '.debhelper', self.parent_package.package_name)

    @property
    def staging_dir(self):
        return os.path.join(self.dbgsym_info_dir, 'dbgsym-root')

    @property
    def substvars(self) -> Substvars:  # pragma: no cover
        raise TypeError("dbgsym packages do not have their own Substvars (use dbgsym.parent_package.substvars instead)")

    @property
    def maintscript_snippets(self) -> FlushableSubstvars:  # pragma: no cover
        raise TypeError("dbgsym packages do not have maintscripts;"
                        " maybe you wanted dbgsym.parent_package.substvars instead?"
                        )

    @property
    def autotriggers(self) -> Triggers:  # pragma: no cover
        raise TypeError("dbgsym packages do not have their own triggers (use dbgsym.parent_package.substvars instead)")


def _check_binary_arch(binary_arch: str, declared_arch: str):
    if binary_arch == 'all':
        return True
    arch_wildcards = declared_arch.split()
    for arch_wildcard in arch_wildcards:
        if debarch_is(binary_arch, arch_wildcard):
            return True
    return False


class BinaryPackage(AbstractBinaryPackage):

    __slots__ = [
        '_package_fields',
        '_dbgsym_binary_package',
        '_should_be_acted_on',
        '_arch_table',
        '_declared_wildcard_matches_run_arch',
        '_is_main_package',
        '_substvars',
        '_maintscript_snippets',
        '_autotriggers',
    ]

    def __init__(self, fields: Union[Dict[str, str], Deb822], arch_table: DpkgArchitectureBuildProcessValuesTable, *,
                 is_main_package=False, should_be_acted_on=True):
        super(BinaryPackage, self).__init__()
        # Typing-wise, Deb822 is *not* a Dict[str, str] but it behaves enough
        # like one that we rely on it and just cast it.
        self._package_fields = typing.cast('Dict[str, str]', fields)
        self._dbgsym_binary_package = None
        self._should_be_acted_on = should_be_acted_on
        self._arch_table = arch_table
        self._is_main_package = is_main_package
        self._substvars = FlushableSubstvars()
        self._maintscript_snippets = MaintscriptSnippets(default_maintscript_lookup)
        self._autotriggers = Triggers()
        self._declared_wildcard_matches_run_arch = _check_binary_arch(
            self.binary_architecture,
            self.declared_architecture
        )

    @property
    def should_be_acted_on(self) -> bool:
        return self._should_be_acted_on and self._declared_wildcard_matches_run_arch

    @property
    def package_fields(self) -> Dict[str, str]:
        return self._package_fields

    @property
    def binary_architecture(self):
        arch = self.declared_architecture
        if arch == 'all':
            return arch
        if self._x_dh_build_for_type == 'target':
            return self._arch_table['DEB_TARGET_ARCH']
        return self._arch_table.current_host_arch

    @property
    def _x_dh_build_for_type(self):
        return self._package_fields.get('X-DH-Build-For-Type', 'host').lower()

    @property
    def dbgsym_binary_package(self) -> Optional[DbgsymBinaryPackage]:
        return self._dbgsym_binary_package

    def create_or_get_dbgsym_binary_package(self) -> DbgsymBinaryPackage:
        if self._dbgsym_binary_package is None:
            if self.is_arch_all:
                raise TypeError("Cannot register a dbgsym package for {}: It is arch:all".format(self.package_name))
            self._dbgsym_binary_package = DbgsymBinaryPackage(self)
        return self._dbgsym_binary_package

    @property
    def package_type(self):
        return self.package_fields.get('Package-Type', DEFAULT_PACKAGE_TYPE)

    @property
    def has_dbgsym_binary_package(self) -> bool:
        return True if self._dbgsym_binary_package is not None else False

    @property
    def is_main_package(self) -> bool:
        return self._is_main_package

    @property
    def substvars(self) -> FlushableSubstvars:
        return self._substvars

    @property
    def maintscript_snippets(self) -> MaintscriptSnippets:
        return self._maintscript_snippets

    @property
    def autotriggers(self) -> Triggers:
        return self._autotriggers

    def cross_command(self, command):
        arch_table = self._arch_table
        if self._x_dh_build_for_type == 'target':
            target_gnu_type = arch_table['DEB_TARGET_GNU_TYPE']
            if arch_table['DEB_HOST_GNU_TYPE'] != target_gnu_type:
                return f"{target_gnu_type}-{command}"
        if arch_table.is_cross_compiling:
            return f"{arch_table['DEB_HOST_GNU_TYPE']}-{command}"
        return command


class SourcePackage:

    __slots__ = ('_package_fields',)

    def __init__(self, fields: Union[Dict[str, str], Deb822]):
        self._package_fields = fields

    @property
    def package_fields(self) -> Dict[str, str]:
        return self._package_fields

    @property
    def package_name(self):
        return self._package_fields['Source']
