import contextlib
import errno
import io
import os
import shutil
import struct
import typing
from typing import Union, Iterable

from deb.debputy import cliutil

if typing.TYPE_CHECKING:
    # Avoid circular import issues that only occurs due to type checking
    from deb.debputy.packages import AbstractBinaryPackage


def package_ext_file(binary_package: 'AbstractBinaryPackage',
                     basename):
    """Similar to debhelper's pkgext except it returns the path rather than the prefix

    Compare:
     * debhelper's pkgext("foo") returns "foo." which would manually have to combined / inserted
       a la: "debian/%s.bar" % pkgext("foo")
     * deputy's package_exit_file(BinaryPackage(package_name="foo", ...), "file") returns "debian/foo.bar"

    Note: This only returns the file path but does not verify that it exist.
    """
    return "debian/%s.%s" % (binary_package.package_name, basename)


def package_file(binary_package: 'AbstractBinaryPackage',
                 basename,
                 *,
                 always_fallback_to_packageless_variant=False,
                 mandatory=False
                 ):
    """Reduced implementation of debhelper's pkgfile

    Looks up the package specific version of a file in debian as debhelper's pkgfile
    would EXCEPT:
     * There is no support for architecture specific files
     * There is no support for --name

    :param binary_package A BinaryPackage
    :param basename Basename of the package file to look up (e.g. "install" or "docs")
    :param always_fallback_to_packageless_variant If True, always look up the package-less variant of
    the path.  This fallback is otherwise only performed for the "main package".
    :param mandatory If True, this will raise a FileNotFoundError if there is no matching file instead
    of returning None.
    :returns A path to the package file or None if the file could not be located (assuming mandatory=False)
    """

    possible_names = [
        "%s.%s" % (binary_package.package_name, basename)
    ]
    if binary_package.is_main_package or always_fallback_to_packageless_variant:
        possible_names.append(basename)

    for name in possible_names:
        path = "debian/%s" % name
        if os.path.exists(path):
            return path
    if mandatory:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), "debian%s" % possible_names[0])
    return None


def install_dirs(*directories):
    missing_dirs = [d for d in directories if not os.path.isdir(d)]
    if not missing_dirs:
        return
    cliutil.log_command('install', '-p', '-m0755', *missing_dirs)
    for directory in missing_dirs:
        # FIXME: install does not respect umask and neither should we here but os.makedirs _does_
        os.makedirs(directory, exist_ok=True)


def install_file(source, dest, *, mode=0o644):
    cliutil.log_command('install', '-p', '-m0{0:o}'.format(mode), source, dest)
    shutil.copymode(source, dest)
    os.chmod(dest, mode)


def generated_file(binary_package: 'Union[AbstractBinaryPackage, str]', filename, *, create_dirs=True):
    try:
        package_name = binary_package.package_name
    except AttributeError:
        package_name = binary_package
        if package_name != '_source':
            raise ValueError("generated_file must receive an AbstractBinaryPackage or \"_source\" as first argument")

    path = os.path.join('debian', '.debhelper', 'generated', package_name, filename)

    if create_dirs:
        directory = os.path.dirname(path)
        install_dirs(directory)
    return path


def reset_perm(mode: int, *paths):
    cliutil.log_command('chmod', f'0{mode:o}', *paths)
    for path in paths:
        os.chmod(path, mode)


ELF_HEADER_SIZE32 = 136
ELF_HEADER_SIZE64 = 232
ELF_MAGIC = b'\x7fELF'
ELF_VERSION = 0x00000001
ELF_ENDIAN_LE = 0x01
ELF_ENDIAN_BE = 0x02
ELF_TYPE_EXECUTABLE = 0x0002
ELF_TYPE_SHARED_OBJECT = 0x0003

ELF_LINKING_TYPE_ANY = None
ELF_LINKING_TYPE_DYNAMIC = True
ELF_LINKING_TYPE_STATIC = False

ELF_EI_ELFCLASS32 = 1
ELF_EI_ELFCLASS64 = 2

ELF_PT_DYNAMIC = 2

ELF_EI_NIDENT = 0x10

# ELF header format:
# typedef struct {
#     unsigned char e_ident[EI_NIDENT];  # <-- 16 / 0x10 bytes
#     uint16_t      e_type;
#     uint16_t      e_machine;
#     uint32_t      e_version;
#     ElfN_Addr     e_entry;
#     ElfN_Off      e_phoff;
#     ElfN_Off      e_shoff;
#     uint32_t      e_flags;
#     uint16_t      e_ehsize;
#     uint16_t      e_phentsize;
#     uint16_t      e_phnum;
#     uint16_t      e_shentsize;
#     uint16_t      e_shnum;
#     uint16_t      e_shstrndx;
# } ElfN_Ehdr;


class IncompleteFileError(RuntimeError):
    pass


def is_so_or_exec_elf_file(path, *, assert_linking_type=ELF_LINKING_TYPE_ANY):
    buffer_size = 4096
    buffer = bytearray(buffer_size)
    fd: io.BufferedReader
    with open(path, 'rb', buffering=io.DEFAULT_BUFFER_SIZE) as fd:
        len_elf_header_raw = fd.readinto(buffer)
        if not buffer or len_elf_header_raw < ELF_HEADER_SIZE32 or not buffer.startswith(ELF_MAGIC):
            return False

        elf_ei_class = buffer[4]
        endian_raw = buffer[5]
        if endian_raw == ELF_ENDIAN_LE:
            endian = '<'
        elif endian_raw == ELF_ENDIAN_BE:
            endian = '>'
        else:
            return False

        if elf_ei_class == ELF_EI_ELFCLASS64:
            offset_size = 'Q'
            # We know it needs to be a 64bit ELF, then the header must be
            # large enough for that.
            if len_elf_header_raw < ELF_HEADER_SIZE64:
                return False
        elif elf_ei_class == ELF_EI_ELFCLASS32:
            offset_size = 'L'
        else:
            return False

        elf_type, elf_machine, elf_version = struct.unpack_from(
            f'{endian}HHL',
            buffer,
            offset=ELF_EI_NIDENT
        )
        if elf_version != ELF_VERSION:
            return False
        if elf_type not in (ELF_TYPE_EXECUTABLE, ELF_TYPE_SHARED_OBJECT):
            return False

        if assert_linking_type is not ELF_LINKING_TYPE_ANY:
            # To check the linking, we look for a DYNAMICALLY program header
            # In other words, we assume static linking by default.

            linking_type = ELF_LINKING_TYPE_STATIC
            # To do that, we need to read a bit more of the ELF header to
            # locate the Program header table.
            #
            # Reading - in order at offset 0x18:
            #  * e_entry (ignored)
            #  * e_phoff
            #  * e_shoff (ignored)
            #  * e_flags (ignored)
            #  * e_ehsize (ignored)
            #  * e_phentsize
            #  * e_phnum
            _, e_phoff, _, _, _, e_phentsize, e_phnum = struct.unpack_from(
                f'{endian}{offset_size}{offset_size}{offset_size}LHHH',
                buffer,
                offset=ELF_EI_NIDENT + 8
            )

            # man 5 elf suggests that Program headers can be absent.  If so,
            # e_phnum will be zero - but we assume the same for e_phentsize.
            if e_phnum == 0:
                return assert_linking_type == linking_type

            # Program headers must be at least 4 bytes for this code to do
            # anything sanely.  In practise, it must be larger than that
            # as well.  Accordingly, at best this is a corrupted ELF file.
            if e_phentsize < 4:
                return False

            fd.seek(e_phoff, os.SEEK_SET)
            unpack_format = f'{endian}L'
            try:
                for program_header_raw in _read_bytes_iteratively(fd, e_phentsize, e_phnum):
                    p_type = struct.unpack_from(unpack_format, program_header_raw)[0]
                    if p_type == ELF_PT_DYNAMIC:
                        linking_type = ELF_LINKING_TYPE_DYNAMIC
                        break
            except IncompleteFileError:
                return False

            return linking_type == assert_linking_type

    return True


def _read_bytes_iteratively(fd, object_size: int, object_count: int):
    total_size = object_size * object_count
    bytes_remaining = total_size
    # FIXME: improve this to read larger chunks and yield them one-by-one
    buffer = bytearray(object_size)

    while bytes_remaining > 0:
        n = fd.readinto(buffer)
        if n != object_size:
            break
        bytes_remaining -= n
        yield buffer

    if bytes_remaining:
        raise IncompleteFileError()


@contextlib.contextmanager
def substvars_flushed(binary_package: 'AbstractBinaryPackage'):
    substvars_file = package_ext_file(binary_package, 'substvars')
    substvars = binary_package.substvars
    substvars.flush_substvars(substvars_file)
    try:
        yield substvars_file
    finally:
        substvars.flush_finished()


@contextlib.contextmanager
def all_substvars_flushed(binary_packages: 'Iterable[AbstractBinaryPackage]'):
    all_substvars = []
    for binary_package in binary_packages:
        substvars_file = package_ext_file(binary_package, 'substvars')
        substvars = binary_package.substvars
        substvars.flush_substvars(substvars_file)
        all_substvars.append(substvars)
    try:
        yield
    finally:
        for substvars in all_substvars:
            substvars.flush_finished()


def initialize_substvars(binary_packages: 'Iterable[AbstractBinaryPackage]'):
    for binary_package in binary_packages:
        substvars_file = package_ext_file(binary_package, 'substvars')
        if os.path.isfile(substvars_file):
            binary_package.substvars.initially_flushed(substvars_file)


def initialize_maintscript_snippets(binary_packages: 'Iterable[AbstractBinaryPackage]'):
    for binary_package in binary_packages:
        ms = binary_package.maintscript_snippets
        ms.load_previous_generated_script(binary_package)


def initialize_triggers(binary_packages: 'Iterable[AbstractBinaryPackage]'):
    for binary_package in binary_packages:
        at = binary_package.autotriggers
        trigger_path = generated_file(binary_package, 'triggers', create_dirs=False)

        try:
            with open(trigger_path) as fd:
                at.read_triggers(fd)
        except FileNotFoundError:
            pass
