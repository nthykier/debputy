import re
import textwrap
from typing import Tuple

from deb.debputy.internal_globals import STANDARD_WARN_ERROR_LOGGER


def _locate_word(line: str, word_no: int, *, until_eol: bool = False):
    if word_no == 1:
        re_str = r'^\s*(\S+)'
    else:
        re_str = r'^\s*(?:\S+\s+){' + str(word_no - 1) + r'}(\S+)'
    m = re.search(re_str, line)
    if m:
        pos = m.span(1)
        if until_eol:
            pos = (pos[0], len(line))
        return pos
    _report_error(f"Could not find word {word_no}", True)
    return None


def _pos_from_re(re_match: re.Match):
    if re_match is not None:
        return re_match.span()
    _report_error("Got None instead of a regex match", True)
    return None


def _report_error(cause: str, should_log_exception: bool):
    # We log but continue here as the we are likely to throw another exception soon that is much more useful
    # to the end user. They cannot fix bugs in debputy / addons but they can fix the bug that triggered
    # the need for this LocationReference
    error_msg = f"Bug in deputy or addon: Invalid attempt to set a position on SingleLineLocationReference: {cause}"
    STANDARD_WARN_ERROR_LOGGER.error(error_msg, exc_info=should_log_exception)


class LineReference:
    __slots__ = ['filename', 'line_number', 'line_content']

    def __init__(self, filename: str, line_number: int, *, content: str = None):
        self.filename = filename
        self.line_number = line_number
        self.line_content = content

    def marking_entire_line(self) -> 'LineReferenceWithPosition':
        return self.marking_start_end_offset(0, len(self.line_content))

    def marking_start_end_offset(self, pos_start: int, pos_end: int) -> 'LineReferenceWithPosition':
        return LineReferenceWithPosition(self.filename, self.line_number, self.line_content, (pos_start, pos_end))

    def marking_offset_tuple(self, pos: Tuple[int, int]) -> 'LineReferenceWithPosition':
        return LineReferenceWithPosition(self.filename, self.line_number, self.line_content, pos)

    def marking_word_no(self, word_number) -> 'LineReferenceWithPosition':
        return self.marking_offset_tuple(_locate_word(self.line_content, word_number, until_eol=False))

    def marking_word_no_and_until_eol(self, word_number) -> 'LineReferenceWithPosition':
        return self.marking_offset_tuple(_locate_word(self.line_content, word_number, until_eol=True))

    def marking_regex_match(self, re_match: re.Match) -> 'LineReferenceWithPosition':
        return self.marking_offset_tuple(_pos_from_re(re_match))


class LineReferenceWithPosition(LineReference):

    __slots__ = ['start_pos', 'end_pos']

    def __init__(self, filename, line_number, content, pos: Tuple[int, int] = None):
        super().__init__(filename, line_number, content=content)
        self.start_pos = None
        self.end_pos = None

        if pos is not None:
            self.start_pos, self.end_pos = pos

    @classmethod
    def loc_ref_entire_line(cls, filename, line_number, content) -> 'LineReferenceWithPosition':
        return LineReferenceWithPosition(filename,
                                         line_number,
                                         content,
                                         (0, len(content))
                                         )

    @classmethod
    def loc_ref_from_word_no(cls, filename, line_number, content, word_number) -> 'LineReferenceWithPosition':
        return LineReferenceWithPosition(filename,
                                         line_number,
                                         content,
                                         _locate_word(content, word_number, until_eol=False),
                                         )

    @classmethod
    def loc_ref_from_word_no_until_eol(cls,
                                       filename,
                                       line_number,
                                       content,
                                       word_number,
                                       ) -> 'LineReferenceWithPosition':
        return LineReferenceWithPosition(filename,
                                         line_number,
                                         content,
                                         _locate_word(content, word_number, until_eol=True),
                                         )

    @classmethod
    def line_ref_from_re_match(cls,
                               filename,
                               line_number,
                               content,
                               re_match: re.Match,
                               ) -> 'LineReferenceWithPosition':
        return LineReferenceWithPosition(filename,
                                         line_number,
                                         content,
                                         _pos_from_re(re_match),
                                         )

    @property
    def has_position(self):
        return self.start_pos is not None or self.end_pos is not None

    def render_error(self, error_message: str):
        if self.line_content is None or self.start_pos is None:
            return f"Error in configuration file {self.filename} line {self.line_number}:" \
                   f" {error_message}"
        if self.end_pos is not None:
            len_of_marker = self.end_pos - self.start_pos
        else:
            len_of_marker = 1
        full_marker = "^".ljust(len_of_marker, '^')
        marker_offset_in_ws = ' '.rjust(self.start_pos, ' ') if self.start_pos else ''
        marker = marker_offset_in_ws + full_marker
        return f"Error in line {self.line_number} of {self.filename}\n" \
               f"\t{self.line_content}\n\t{marker}\n  {error_message}"


class InvalidConfigurationFileInputError(Exception):

    def __init__(self, error_msg, location_ref: LineReferenceWithPosition):
        super(InvalidConfigurationFileInputError, self).__init__(error_msg)
        self.location_ref = location_ref


class UnknownPackageError(ValueError):

    def __init__(self, msg: str, option_name: str, *package_names: str):
        super().__init__(msg)
        self._option_name = option_name
        self._package_names = package_names

    @property
    def package_names(self):
        return self._package_names

    @property
    def option_name(self):
        return self._option_name


class InvalidDebianControlFileError(RuntimeError):
    pass


class MissingMandatoryFieldInDebianControlFileError(InvalidDebianControlFileError):

    def __init__(self, message, field_name, paragraph_index, paragraph_name):
        super().__init__(message)
        self._field_name = field_name
        self._paragraph_index = paragraph_index
        self._paragraph_name = paragraph_name


class DebhelperCompatLevelError(RuntimeError):
    pass


class InvalidDebhelperCompatLevelSpecificationError(DebhelperCompatLevelError):
    pass


class UnsupportedDebhelperCompatLevelError(DebhelperCompatLevelError):
    pass


class BugInDebputyOrAddonError(Exception):

    def human_readable_description(self):
        return str(self)


class AutoscriptSnippetNotFoundError(BugInDebputyOrAddonError):

    def human_readable_description(self):
        return textwrap.dedent("""\
        Could not locate autoscript snippet {args[1]}.

        Tried in the following search path:
        {SEARCH_PATH}

        Most likely this is a bug in debputy OR a third-party add-on that
        either has a typo in the snippet name or forgot to provide it in
        the above search path.
        """).format(
            args=self.args,
            SEARCH_PATH="\n".join(f" * {p}" for p in self.args[2])
        )
