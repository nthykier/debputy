#!/usr/bin/python3

import argparse
import asyncio
import logging
import os
import sys
from typing import Set

import deb.debputy.dht.builddeb as builddeb
import deb.debputy.dht.gencontrol as gencontrol
import deb.debputy.dht.installdeb as installdeb
import deb.debputy.dht.makeshlibs as makeshlibs
import deb.debputy.dht.md5sums as md5sums
import deb.debputy.dht.shlibdeps as shlibdeps
from deb.debputy import initialize_debputy_from_parsed_args
from deb.debputy.buildreq import DebputyInvocationContext
from deb.debputy.exceptions import InvalidConfigurationFileInputError, BugInDebputyOrAddonError, UnknownPackageError


async def _run_step(module, build_req: DebputyInvocationContext, parallel_task_limit: asyncio.Semaphore):
    pending_cmds = []
    for pkg in build_req.binary_packages:
        if pkg.should_be_acted_on:
            pending_cmds.append(module.process_package_for_sysusers(pkg, build_req, parallel_task_limit))
    await asyncio.gather(*pending_cmds)


async def run_dpkg_gensymbols(*args):
    await makeshlibs.process_all_packages(*args)


async def run_dpkg_shlibdeps_async(*args):
    await _run_step(shlibdeps, *args)


async def run_dpkg_installdeb_async(*args):
    await _run_step(installdeb, *args)


async def run_dpkg_gencontrol_async(*args):
    await _run_step(gencontrol, *args)


async def generate_md5sums_async(*args):
    await _run_step(md5sums, *args)


async def run_dpkg_deb_build_async(*args):
    await _run_step(builddeb, *args)


def main():
    logging.basicConfig(level=logging.INFO, format='%(message)s')

    parser = argparse.ArgumentParser(description='Debputy package helper prototype.')
    parser.add_argument('-p', '--package', dest='selected_packages',
                        action='append',
                        help='Act on the package named package. This option may'
                             ' be specified multiple times to make debputy'
                             ' operate on a given set of packages.')

    parser.add_argument('-N', '--no-package', dest='excluded_packages',
                        action='append',
                        default=[],
                        help='Do not act on the specified package even if an'
                             ' -a, -i, or -p option lists the package as one'
                             ' that should be acted on.')

    parser.add_argument('-i', '--indep', dest='arch_all',
                        action='store_true',
                        help='Act on all architecture independent packages')

    parser.add_argument('-a', '--arch', dest='arch_any',
                        action='store_true',
                        help='Act on architecture dependent packages that should'
                             ' be built for the DEB_HOST_ARCH architecture.')

    parser.set_defaults(arch_all=False, arch_any=False,
                        selected_packages=[],
                        excluded_packages=[])

    args = parser.parse_args()

    args.excluded_packages = set(args.excluded_packages)
    args.selected_packages = set(args.selected_packages)

    try:
        build_req = initialize_debputy_from_parsed_args(args.selected_packages,
                                                        args.excluded_packages,
                                                        args.arch_all,
                                                        args.arch_any,
                                                        )
    except UnknownPackageError as e:
        stop_with_message(e.args[0], exit_code=0)
        raise

    if not any(p.should_be_acted_on for p in build_req.binary_packages):
        stop_with_message("No packages to be acted on...")

    try:
        asyncio.run(main_async(build_req))
        sys.exit(0)
    except InvalidConfigurationFileInputError as e:
        stop_with_message(e.location_ref.render_error(e.args[0]))
    except BugInDebputyOrAddonError as e:
        stop_with_message(e.human_readable_description())


def check_package_sets(provided_packages: Set[str], valid_package_names: Set[str], option_name: str):
    if not (provided_packages < valid_package_names):
        non_existing_packages = provided_packages - valid_package_names
        invalid_package_list = ", ".join(sorted(non_existing_packages))

        stop_with_message(f'Invalid package names passed to {option_name}: {invalid_package_list}',
                          f'Valid package names are: {", ".join(valid_package_names)}'
                          )


def _error_prefix():
    return f"{os.path.basename(sys.argv[0])}: error: "


def stop_with_message(*messages: str, exit_code=1):
    error_prefix = _error_prefix()
    if len(messages) == 1 and '\n' in messages[0]:
        messages = messages[0].rstrip().splitlines()
    for m in messages:
        print(error_prefix + m, file=sys.stderr)
    sys.exit(exit_code)


async def main_async(build_req: DebputyInvocationContext):

    parallel_task_limit = asyncio.Semaphore(3)

    sequence = [
        run_dpkg_gensymbols,
        run_dpkg_shlibdeps_async,
        run_dpkg_installdeb_async,
        run_dpkg_gencontrol_async,
        generate_md5sums_async,
        run_dpkg_deb_build_async
    ]

    for seq in sequence:
        await seq(build_req, parallel_task_limit)


if __name__ == '__main__':
    main()
